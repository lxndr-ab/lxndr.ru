data "docker_registry_image" "couchdb" {
  name = "couchdb:3"
}

resource "docker_volume" "couchdb_data" {
  name = "lxndrru_couchdb-data"

  lifecycle {
    prevent_destroy = true
  }

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }
}

resource "docker_service" "couchdb" {
  name    = "lxndrru_couchdb"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }

  task_spec {
    networks = [docker_network.lxndrru.id]

    container_spec {
      image = "${data.docker_registry_image.couchdb.name}@${data.docker_registry_image.couchdb.sha256_digest}"
      hostname = "couchdb"

      env = var.couchdb_env

      mounts {
        target    = "/opt/couchdb/data"
        source    = docker_volume.couchdb_data.name
        type      = "volume"
        read_only = false
      }

      healthcheck {
        test = ["CMD", "curl", "--silent", "--output", "/dev/null", "--fail", "http://localhost:5984/"]
      }
    }
  }

  endpoint_spec {
    ports {
      target_port = "5984"
      published_port = "5984"
    }
  }
}
