variable "postgres_env" {
  description = "postgres config"
  type        = object({
    POSTGRES_PASSWORD  = string
  })
}
