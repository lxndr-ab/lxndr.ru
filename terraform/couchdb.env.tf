variable "couchdb_env" {
  description = "couchdb config"
  type        = object({
    COUCHDB_USER = string
    COUCHDB_PASSWORD  = string
  })
}
