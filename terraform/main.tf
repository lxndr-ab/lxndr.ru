terraform {
  backend "http" {
  }

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.17.0"
    }
  }
}

provider "docker" {
  host = "tcp://lxndr.ru:2376/"
  cert_path = "${path.cwd}/certs"
}

resource "docker_network" "lxndrru" {
  name   = "lxndrru"
  driver = "overlay"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }
}

data "docker_network" "awsm" {
  name = "awsm"
}

data "docker_network" "stash" {
  name = "stash_default"
}

module "gaming" {
  source = "../apps/gaming/terraform"
  network_id = docker_network.lxndrru.id
}

module "gtbot" {
  source = "../apps/gtbot/terraform"
  network_id = docker_network.lxndrru.id
  gtbot_env = var.gtbot_env
}

module "home" {
  source = "../apps/home/terraform"
  network_id = docker_network.lxndrru.id
}

module "roll" {
  source = "../apps/roll/terraform"
  network_id = docker_network.lxndrru.id
  roll_env = var.roll_env
}
