data "docker_registry_image" "postgres" {
  name = "postgres:14"
}

resource "docker_volume" "postgres_data" {
  name = "lxndrru_postgres-data"

  lifecycle {
    prevent_destroy = true
  }

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }
}

resource "docker_service" "postgres" {
  name    = "lxndrru_postgres"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }

  task_spec {
    networks = [docker_network.lxndrru.id]

    container_spec {
      image = "${data.docker_registry_image.postgres.name}@${data.docker_registry_image.postgres.sha256_digest}"
      hostname = "postgres"

      env = var.postgres_env

      mounts {
        target    = "/var/lib/postgresql/data"
        source    = docker_volume.postgres_data.name
        type      = "volume"
        read_only = false
      }
    }
  }

  endpoint_spec {
    ports {
      target_port = "5432"
      published_port = "5432"
    }
  }
}
