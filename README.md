# renew ssl

for x86_64

```sh
docker run -it --rm -v rproxy_certbot-data:/etc/letsencrypt certbot/certbot:latest certonly --keep-until-expiring --expand --manual --manual-public-ip-logging-ok --email lxndr87i@gmail.com --agree-tos --no-eff-email --preferred-challenges dns --domain "lxndr.ru" --domain "*.lxndr.ru"
```

for ARMv7

```sh
docker run -it --rm -v rproxy_certbot-data:/etc/letsencrypt casperfrx/certbot:latest certonly --keep-until-expiring --expand --manual --manual-public-ip-logging-ok --email lxndr87i@gmail.com --agree-tos --no-eff-email --preferred-challenges dns --domain "lxndr.ru" --domain "*.lxndr.ru"
```

to check DNS propogation

```sh
https://dns.google.com/query?name=_acme-challenge.lxndr.ru&type=TXT&dnssec=false
```

# backup letsencrypt

```sh
docker run --rm -it -v lxndrru_certbot-data:/etc/letsencrypt -v ~/backups:/backups alpine sh -c "cd /etc/letsencrypt/ && tar czvf /backups/letsencrypt.tar.gz ."
```

```sh
scp -o StrictHostKeyChecking=no ec2-user@lxndr.ru:/home/ec2-user/lxndr.ru/backups/letsencrypt.tar.gz ./
```

# restore letsencrypt

```sh
docker volume create lxndrru_certbot-data
```

```sh
docker run --rm -v lxndrru_certbot-data:/etc/letsencrypt -v ~/backups:/backups alpine sh -c "cd /etc/letsencrypt && tar xvf /backups/letsencrypt.tar.gz"
```
