terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.17.0"
    }
  }
}

variable "network_id" {
  description = "Network ID"
  type        = string
}
