data "docker_registry_image" "home" {
  name = "registry.gitlab.com/lxndr-ab/lxndr.ru/home:latest"
}

locals {
  roll_labels = {
    "com.docker.stack.namespace"                                = "lxndrru"
    "traefik.enable"                                            = "true"
    "traefik.http.services.lxndr_home.loadBalancer.server.port" = "80"
    "traefik.http.routers.lxndr_home.rule"                      = "Host(`lxndr.ru`)"
    "traefik.http.routers.lxndr_home.middlewares"               = "redirect-to-https@file"
    "traefik.http.routers.lxndr_home.tls"                       = "true"
  }
}

resource "docker_service" "home" {
  name = "lxndrru_home"

  dynamic "labels" {
    for_each = local.roll_labels
    content {
      label = labels.key
      value = labels.value
    }
  }

  task_spec {
    networks = [var.network_id]

    container_spec {
      image = "${data.docker_registry_image.home.name}@${data.docker_registry_image.home.sha256_digest}"

      healthcheck {
        test = ["CMD", "wget", "--quiet", "--spider", "http://localhost:80/"]
      }
    }
  }
}
