module gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/golang/glog v1.0.0
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/net v0.0.0-20220622184535-263ec571b305 // indirect
)
