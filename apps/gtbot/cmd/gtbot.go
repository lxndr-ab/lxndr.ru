package main

import (
	"flag"
	"os"

	"github.com/golang/glog"
	_ "github.com/joho/godotenv/autoload"
	telegram "gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/notifier-telegram"
	"gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/storage"
	gametracker "gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/watcher-gametracker"
)

func main() {
	flag.Parse()

	dbHost := os.Getenv("REDIS_HOST")
	botToken := os.Getenv("TGBOT_TOKEN")

	s := storage.NewRedisStorage(dbHost)
	w := gametracker.NewGametrackerWatcher(s)
	n := telegram.NewTelegramNotifier(botToken, s)

	for ent := range w.C {
		glog.Infof("Found \"%s\" %s %s", ent.PlayerName, ent.Game, ent.ServerIP)

		ignored := s.IsNameIgnored(ent.ChatID, ent.PlayerName) ||
			s.IsGameIgnored(ent.ChatID, ent.Game) ||
			s.IsServerIgnored(ent.ChatID, ent.ServerIP)

		if ignored == true {
			continue
		}

		err := n.Send(ent)

		if err != nil {
			glog.Errorf("Error sending a message %s:", err.Error())
		}
	}
}
