package storage

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-redis/redis/v8"
	"github.com/golang/glog"
)

// Storage is a Redis storage
type Storage struct {
	ctx    context.Context
	client *redis.Client
}

// NewRedisStorage creates a new Redis storage instance
func NewRedisStorage(host string) *Storage {
	client := redis.NewClient(&redis.Options{
		Addr:     host,
		Password: "",
		DB:       0,
	})

	return &Storage{
		ctx:    context.Background(),
		client: client,
	}
}

// GetChats gets all the chats
func (s *Storage) GetChats() []int64 {
	ids, err := s.client.SMembers(s.ctx, "chats").Result()

	if err != nil {
		glog.Fatalf("Failed to execute 'SMEMBERS chats': %s", err.Error())
	}

	var res []int64

	for _, x := range ids {
		id, _ := strconv.ParseInt(x, 10, 64)
		res = append(res, id)
	}

	return res
}

// AddChat adds telegram chat
func (s *Storage) AddChat(chatID int64) {
	err := s.client.SAdd(s.ctx, "chats", chatID).Err()

	if err != nil {
		glog.Fatalf("Failed to execute 'SADD chats': %s", err.Error())
	}
}

// RemoveChat removes chat
func (s *Storage) RemoveChat(chatID int64) {
	err := s.client.SRem(s.ctx, "chats", chatID).Err()

	if err != nil {
		glog.Fatalf("Failed to execute 'SREM chats': %s", err.Error())
	}
}

// GetNames gets all the names to watch
func (s *Storage) GetNames(chatID int64) []string {
	return s.getItemsFromSet(chatID, "names")
}

// AddName adds player
func (s *Storage) AddName(chatID int64, name string) {
	s.addToSet(chatID, "names", name)
}

// RemoveName removes player
func (s *Storage) RemoveName(chatID int64, name string) {
	s.removeFromSet(chatID, "names", name)
}

// GetIgnoredGames gets all the games to ignore
func (s *Storage) GetIgnoredGames(chatID int64) []string {
	return s.getItemsFromSet(chatID, "ignored-games")
}

// IsGameIgnored checks if game must be ignored
func (s *Storage) IsGameIgnored(chatID int64, name string) bool {
	return s.isItemInSet(chatID, "ignored-games", name)
}

// IgnoreGame adds game to ignore
func (s *Storage) IgnoreGame(chatID int64, name string) {
	s.addToSet(chatID, "ignored-games", name)
}

// UnignoreGame removes game to ignore
func (s *Storage) UnignoreGame(chatID int64, name string) {
	s.removeFromSet(chatID, "ignored-games", name)
}

// GetIgnoredNames gets all ignored names
func (s *Storage) GetIgnoredNames(chatID int64) []string {
	return s.getItemsFromSet(chatID, "ignored-names")
}

// IsNameIgnored checks if name must be ignored
func (s *Storage) IsNameIgnored(chatID int64, name string) bool {
	return s.isItemInSet(chatID, "ignored-names", name)
}

// IgnoreName adds name to ignore
func (s *Storage) IgnoreName(chatID int64, name string) {
	s.addToSet(chatID, "ignored-names", name)
}

// UnignoreName removes name to ignore
func (s *Storage) UnignoreName(chatID int64, name string) {
	s.removeFromSet(chatID, "ignored-names", name)
}

// GetIgnoredServers gets all ignored servers
func (s *Storage) GetIgnoredServers(chatID int64) []string {
	return s.getItemsFromSet(chatID, "ignored-servers")
}

// IsServerIgnored checks if server must be ignored
func (s *Storage) IsServerIgnored(chatID int64, name string) bool {
	return s.isItemInSet(chatID, "ignored-servers", name)
}

// IgnoreServer adds server to ignore
func (s *Storage) IgnoreServer(chatID int64, name string) {
	s.addToSet(chatID, "ignored-servers", name)
}

// UnignoreServer removes server to ignore
func (s *Storage) UnignoreServer(chatID int64, name string) {
	s.removeFromSet(chatID, "ignored-servers", name)
}

func (s *Storage) getItemsFromSet(chatID int64, setName string) []string {
	key := fmt.Sprintf("chat:%d:%s", chatID, setName)
	items, err := s.client.SMembers(s.ctx, key).Result()

	if err != nil {
		glog.Fatalf("Failed to execute 'SMEMBERS %s': %s", key, err.Error())
	}

	return items
}

func (s *Storage) isItemInSet(chatID int64, setName string, itemName string) bool {
	key := fmt.Sprintf("chat:%d:%s", chatID, setName)
	exists, err := s.client.SIsMember(s.ctx, key, itemName).Result()

	if err != nil {
		glog.Fatalf("Failed to execute 'SISMEMBER %s': %s", key, err.Error())
	}

	return exists
}

func (s *Storage) addToSet(chatID int64, setName string, itemName string) {
	key := fmt.Sprintf("chat:%d:%s", chatID, setName)
	err := s.client.SAdd(s.ctx, key, itemName).Err()

	if err != nil {
		glog.Fatalf("Failed to execute 'SADD %s': %s", key, err.Error())
	}
}

func (s *Storage) removeFromSet(chatID int64, setName string, itemName string) {
	key := fmt.Sprintf("chat:%d:%s", chatID, setName)
	err := s.client.SRem(s.ctx, key, itemName).Err()

	if err != nil {
		glog.Fatalf("Failed to execute 'SREM %s': %s", key, err.Error())
	}
}
