package gametracker

import (
	"fmt"
	"net/url"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/golang/glog"

	"gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/storage"
	"gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/watcher"
)

// Watcher is gametracker.com watcher implementation
type Watcher struct {
	ticker  *time.Ticker
	storage *storage.Storage
	C       chan watcher.Entry
}

const (
	queryURL      = "https://www.gametracker.com/search/?search_by=online_player&query=%s"
	querySelector = ".table_lst.table_lst_sop tr:not(:first-child):not(:last-child)"
)

// NewGametrackerWatcher creates a new gametracker.com watcher instance
func NewGametrackerWatcher(storage *storage.Storage) *Watcher {
	w := &Watcher{
		ticker:  time.NewTicker(5 * time.Minute),
		storage: storage,
		C:       make(chan watcher.Entry),
	}

	go func() {
		w.tick()

		for range w.ticker.C {
			w.tick()
		}
	}()

	return w
}

func (w *Watcher) tick() {
	for _, chatID := range w.storage.GetChats() {
		for _, name := range w.storage.GetNames(chatID) {
			time.Sleep(2 * time.Second) // a bit dirty. i should not bother GT too much

			for _, entry := range requestPlayerInfo(name) {
				entry.ChatID = chatID
				w.C <- entry
			}
		}
	}
}

func requestPlayerInfo(playerName string) []watcher.Entry {
	uri := fmt.Sprintf(queryURL, url.PathEscape(playerName))
	var entries []watcher.Entry

	doc, err := makeRequest(uri)

	if err != nil {
		glog.Warningf("%v", err)
		return entries
	}

	doc.Find(querySelector).Each(func(i int, s *goquery.Selection) {
		entry := extractPlayerInfo(s)

		if entry != nil {
			entries = append(entries, *entry)
		}
	})

	return entries
}

func extractPlayerInfo(sel *goquery.Selection) *watcher.Entry {
	iconHref, _ := sel.Find("td:nth-child(1) a").Attr("href")
	nameText := sel.Find("td:nth-child(2) a").Text()
	serverHref, _ := sel.Find("td:nth-child(4) a").Attr("href")
	serverText := sel.Find("td:nth-child(4) a").Text()

	serverIP := extractServerIP(serverHref)

	if serverIP == "" {
		return nil
	}

	return &watcher.Entry{
		PlayerName: normalizePlayerName(nameText),
		Game:       extractGameCode(iconHref),
		ServerIP:   serverIP,
		ServerName: serverText,
	}
}
