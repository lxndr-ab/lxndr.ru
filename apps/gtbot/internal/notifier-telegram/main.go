package telegram

import (
	"fmt"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/golang/glog"
	"gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/storage"
	"gitlab.com/lxndr-ab/lxndr.ru/apps/gtbot/internal/watcher"
)

// Notifier is Telegram notifier implementation
type Notifier struct {
	bot     *tgbotapi.BotAPI
	storage *storage.Storage
}

// NewTelegramNotifier creates a new Telegram notifier instance
func NewTelegramNotifier(botToken string, storage *storage.Storage) *Notifier {
	bot, err := tgbotapi.NewBotAPI(botToken)

	if err != nil {
		glog.Fatalf("Failed to connect to telegram bot: %v", err.Error())
	}

	notifier := &Notifier{
		bot:     bot,
		storage: storage,
	}

	glog.Infof("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	go func() {
		for update := range updates {
			msg := update.Message

			if msg != nil && msg.IsCommand() {
				notifier.processCommand(msg.Chat.ID, msg.Command(), msg.CommandArguments())
			}
		}
	}()

	return notifier
}

func (n *Notifier) processCommand(chatID int64, cmd string, argsFull string) {
	switch cmd {
	case "start":
		n.storage.AddChat(chatID)
	case "stop":
		n.storage.RemoveChat(chatID)
	case "add":
		args := strings.SplitN(argsFull, " ", 2)

		if len(args) >= 2 {
			switch args[0] {
			case "name":
				n.storage.AddName(chatID, args[1])
			}
		}
	case "remove":
		args := strings.SplitN(argsFull, " ", 2)

		if len(args) >= 2 {
			switch args[0] {
			case "name":
				n.storage.RemoveName(chatID, args[1])
			}
		}
	case "ignore":
		args := strings.SplitN(argsFull, " ", 2)

		if len(args) >= 2 {
			switch args[0] {
			case "name":
				n.storage.IgnoreName(chatID, args[1])
			case "game":
				n.storage.IgnoreGame(chatID, args[1])
			case "server":
				n.storage.IgnoreServer(chatID, args[1])
			}
		}
	case "unignore":
		args := strings.SplitN(argsFull, " ", 2)

		if len(args) >= 2 {
			switch args[0] {
			case "name":
				n.storage.UnignoreName(chatID, args[1])
			case "game":
				n.storage.UnignoreGame(chatID, args[1])
			case "server":
				n.storage.UnignoreServer(chatID, args[1])
			}
		}
	case "info":
		n.printInfo(chatID)
	}
}

// Send sends an entry
func (n *Notifier) Send(ent watcher.Entry) error {
	txt := fmt.Sprintf("(%s) %s\n%s\n%s", ent.Game, ent.PlayerName, ent.ServerIP, ent.ServerName)
	msg := tgbotapi.NewMessage(ent.ChatID, txt)
	_, err := n.bot.Send(msg)

	if err != nil {
		if err.Error() == "Forbidden: bot was blocked by the user" {
			n.storage.RemoveChat(ent.ChatID)
		}

		return err
	}

	return nil
}

func (n *Notifier) printInfo(chatID int64) {
	names := n.storage.GetNames(chatID)
	ignoredNames := n.storage.GetIgnoredNames(chatID)
	ignoredGames := n.storage.GetIgnoredGames(chatID)
	ignoredServers := n.storage.GetIgnoredServers(chatID)

	txt := fmt.Sprintf(
		"*Names:* %s\n*Ignored names:* %s\n*Ignored games:* %s\n*Ignored servers:* %s",
		strings.Join(names, ", "),
		strings.Join(ignoredNames, ", "),
		strings.Join(ignoredGames, ", "),
		strings.Join(ignoredServers, ", "),
	)

	msg := tgbotapi.NewMessage(chatID, txt)
	_, err := n.bot.Send(msg)

	if err != nil {
		glog.Errorf("Error sending telegram message: %s", err.Error())
	}
}
