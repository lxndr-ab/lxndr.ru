variable "gtbot_env" {
  type        = object({
    TGBOT_TOKEN = string
    REDIS_HOST  = string
  })
}
