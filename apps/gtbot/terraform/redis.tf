data "docker_registry_image" "redis" {
  name = "redis:latest"
}

resource "docker_volume" "gtbot_data" {
  name = "lxndrru_gtbot-data"

  lifecycle {
    prevent_destroy = true
  }

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }
}

resource "docker_service" "gtbot-redis" {
  name     = "lxndrru_gtbot-redis"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }

  task_spec {
    networks = [var.network_id]

    container_spec {
      image = "${data.docker_registry_image.redis.name}@${data.docker_registry_image.redis.sha256_digest}"
      hostname = "gtbot-redis"

      mounts {
        type = "volume"
        read_only = false
        source = docker_volume.gtbot_data.name
        target = "/data"
      }

      healthcheck {
         test = ["CMD", "redis-cli", "ping"]
      }
    }
  }
}
