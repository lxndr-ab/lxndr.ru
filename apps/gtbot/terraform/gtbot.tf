data "docker_registry_image" "gtbot" {
  name = "registry.gitlab.com/lxndr-ab/lxndr.ru/gtbot:latest"
}

resource "docker_service" "gtbot" {
  name     = "lxndrru_gtbot"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }

  task_spec {
    networks = [var.network_id]

    container_spec {
      image = "${data.docker_registry_image.gtbot.name}@${data.docker_registry_image.gtbot.sha256_digest}"
      env = var.gtbot_env
    }
  }
}
