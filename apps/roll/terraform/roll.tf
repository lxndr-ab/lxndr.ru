data "docker_registry_image" "roll" {
  name = "registry.gitlab.com/lxndr-ab/lxndr.ru/roll:latest"
}

locals {
  roll_labels = {
    "com.docker.stack.namespace"                                = "lxndrru"
    "traefik.enable"                                            = "true"
    "traefik.http.services.lxndr_roll.loadbalancer.server.port" = "8443"
    "traefik.http.routers.lxndr_roll.rule"                      = "Host(`roll.lxndr.ru`)"
    "traefik.http.routers.lxndr_roll.middlewares"               = "redirect-to-https@file"
    "traefik.http.routers.lxndr_roll.tls"                       = "true"
  }
}

resource "docker_service" "roll" {
  name = "lxndrru_roll"

  dynamic "labels" {
    for_each = local.roll_labels
    content {
      label = labels.key
      value = labels.value
    }
  }

  task_spec {
    networks = [var.network_id]

    container_spec {
      image = "${data.docker_registry_image.roll.name}@${data.docker_registry_image.roll.sha256_digest}"
      env   = var.roll_env

      healthcheck {
        test = ["CMD", "wget", "--quiet", "--spider", "http://localhost:8443/"]
      }
    }
  }
}
