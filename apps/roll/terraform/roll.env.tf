variable "roll_env" {
  type        = object({
    HOST             = string
    JWT_SECRET       = string
    JWT_ISSUER       = string
    STEAM_WEBAPI_KEY = string
    DB_HOST          = string
    DB_NAME          = string
    DB_USER          = string
    DB_PASS          = string
    ADMIN_STEAMID    = string
  })
}
