/* eslint-disable @typescript-eslint/promise-function-async */

import { TaskManager } from '~/be/TaskManager'
import getCategories from './get-categories'
import getGenres from './get-genres'
import getApps from './get-apps'
import updater from './updater'
import { App, Category, Genre, SteamID } from '~/cm/types'

const taskManager = new TaskManager()

export const shutdown = (): void =>
  taskManager.shutdown()

export const enqueueUpdater = (): Promise<void> =>
  taskManager
    .enqueueTask(updater, { key: 'updater' })
    .then(shouldContinue => {
      if (shouldContinue) {
        void enqueueUpdater()
      }
    })

export const getAppsTask = (steamid: SteamID): Promise<App[]> =>
  taskManager.enqueueTask(
    () => getApps(steamid),
    { key: `get-apps:${steamid}` },
  )

export const getCategoriesTask = (): Promise<Category[]> =>
  taskManager.enqueueTask(
    () => getCategories(),
    { key: 'get-categories' },
  )

export const getGenresTask = (): Promise<Genre[]> =>
  taskManager.enqueueTask(
    () => getGenres(),
    { key: 'get-genres' },
  )
