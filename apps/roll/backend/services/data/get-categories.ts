import _ from 'lodash'
import { MangoQuery } from 'nano'
import * as db from '~/be/db/client'
import * as steam from '~/be/steam'
import { Category } from '~/cm/types'
import { AppDetailsEntity, STEAM_APP_DETAILS } from '~/be/services/steam-data/update-app-details'

const blocklist: string[] = [
  'Additional High-Quality Audio',
  'Includes Source SDK',
  'Stats',
  'Steam Achievements',
  'Steam Turn Notifications',
]

let updatedAt = 0
let categories: Category[] = []

export const getCategories = async (): Promise<Category[]> => {
  const newBatch: steam.Category[][] = []

  const query: MangoQuery = {
    selector: {
      'type': STEAM_APP_DETAILS,
      'updatedAt': {
        $gt: updatedAt,
      },
      'data.categories': {
        $type: 'array',
      },
    },
    fields: [
      'data.categories',
    ],
    use_index: ['apps-index', 'categories'],
  }

  for await (const details of db.find<AppDetailsEntity>(query)) {
    if (details.data?.categories) {
      newBatch.push(details.data.categories)
    }
  }

  if (!newBatch.length) {
    return categories
  }

  categories = _(newBatch)
    .flatten()
    .uniqBy('id')
    .map<Category>(({ id, description }) => ({ id: Number(id), name: description }))
    .concat(categories)
    .uniqBy('id')
    .filter(({ name }) => !blocklist.includes(name))
    .sortBy('name')
    .value()

  updatedAt = Date.now()
  return categories
}

export default getCategories
