/* eslint-disable @typescript-eslint/promise-function-async */

import { AppID, SteamID } from '~/cm/types'
import config from '~/be/config'
import { TaskManager } from '~/be/TaskManager'
import { enqueueUpdater } from '../data'
import udpateAppDetails from './update-app-details'
import updateAppSchema from './update-app-schema'
import { updateApps } from './update-apps'
import updateUserGames from './update-user-games'
import updateUserStats from './update-user-stats'

const taskManager = new TaskManager({
  concurrency: config.steam.requestConcurrency,
})

export const shutdown = (): void =>
  taskManager.shutdown()

export const udpateAppDetailsTask = (appid: AppID): Promise<void> =>
  taskManager.enqueueTask(
    () => udpateAppDetails(appid),
    { key: `update:steam.app.details:${appid}` },
  )

export const updateAppSchemaTask = (appid: AppID): Promise<void> =>
  taskManager.enqueueTask(
    () => updateAppSchema(appid),
    { key: `update:steam.app.schema:${appid}` },
  )

export const updateAppsTask = (): Promise<void> =>
  taskManager.enqueueTask(
    async () => {
      await updateApps()
      void enqueueUpdater()
    },
    { key: 'update:steam.apps' },
  )

export const updateUserGamesTask = (steamid: SteamID): Promise<void> =>
  taskManager.enqueueTask(
    async () => {
      await updateUserGames(steamid)
      void enqueueUpdater()
    },
    { key: `update:steam.user.games:${steamid}` },
  )

export const updateUserStatsTask = (steamid: SteamID, appid: AppID): Promise<void> =>
  taskManager.enqueueTask(
    () => updateUserStats(steamid, appid),
    { key: `update:steam.user.stats:${steamid}-${appid}` },
  )
