import { AxiosError } from 'axios'
import { AppID, SteamID } from '~/cm/types'
import * as steam from '~/be/steam'
import * as db from '~/be/db/client'
import { Entity, Meta } from '~/be/db/types'
import { logger } from '~/be/logger'

export const STEAM_USER_STATS = 'steam.user.stats'

interface UserStatsMeta extends Meta {
  needsUpdate: boolean
  steamid: SteamID
  appid: AppID
  request: {
    ok: boolean
    error: string | null
  }
}

export type UserStatsEntity = Entity<typeof STEAM_USER_STATS, steam.UserStats, UserStatsMeta>

const updateUserStats = async (steamid: SteamID, appid: AppID): Promise<void> => {
  const id = db.formatID(STEAM_USER_STATS, steamid, appid)

  const meta: UserStatsMeta = {
    needsUpdate: false,
    steamid,
    appid,
    request: {
      ok: true,
      error: null,
    },
  }

  try {
    const stats = await steam.getUserStatsForGame({ steamid, appid })
    await db.insertEntity(id, stats, meta)
  } catch (err: unknown) {
    if (err instanceof Error) {
      logger.warn(`steam.getUserStatsForGame({ steamid: "${steamid}", appid: ${appid} }) errored: ${err.message}`)

      if ('isAxiosError' in err) {
        const reqError = err as AxiosError

        const stats: steam.UserStats = {
          achievements: [],
          stats: [],
        }

        await db.errorEntity(id, stats, {
          ...meta,
          request: {
            ok: false,
            error: reqError.message,
          },
        })

        return
      }
    }

    throw err
  }
}

export default updateUserStats
