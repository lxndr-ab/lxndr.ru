import { Middleware } from 'koa-compose'
import { RouterContext } from '@koa/router'
import { ParsedUrlQuery } from 'querystring'
import { User } from './user'

export interface TokenPayload {
  steamid: string
}

export interface KoaState {
  user: User
}

export interface KoaContext<
  Q extends ParsedUrlQuery = ParsedUrlQuery,
> extends RouterContext<KoaState> {
  body: unknown
  query: Q
}

export type KoaMiddleware<
  Q extends ParsedUrlQuery = ParsedUrlQuery,
> = Middleware<KoaContext<Q>>
