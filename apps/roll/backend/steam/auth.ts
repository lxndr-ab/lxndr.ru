/* eslint-disable @typescript-eslint/return-await */
import http from 'http'
import openid from 'openid'
import { SteamID } from '~/cm/types'
import config from '../config'

const openidUrl = 'https://steamcommunity.com/openid'
const steamidRe = /https:\/\/steamcommunity\.com\/openid\/id\/(\d+)/u

const relyingParty = new openid.RelyingParty(config.steam.authReturnUrl, null, true, true, [])

export const authenticate = async (): Promise<string> =>
  new Promise<string>((resolve, reject) => {
    relyingParty.authenticate(openidUrl, false, (err, authUrl) => {
      if (err) {
        return reject(new Error(err.message))
      }

      if (!authUrl) {
        return reject(new Error('no URL received'))
      }

      return resolve(authUrl)
    })
  })

export const verify = async (req: http.IncomingMessage): Promise<SteamID> =>
  new Promise<SteamID>((resolve, reject) => {
    relyingParty.verifyAssertion(req, (err, result) => {
      if (err) {
        return reject(new Error(err.message))
      }

      if (!(result?.authenticated && result.claimedIdentifier)) {
        return reject(new Error('Failed to authenticate by Steam'))
      }

      const m = steamidRe.exec(result.claimedIdentifier)

      if (!m) {
        return reject(new Error('Failed to parse Steam response'))
      }

      return resolve(m[1])
    })
  })
