import { AppID, SteamID } from '~/cm/types'
import { fetch } from './request'

/* eslint-disable @typescript-eslint/naming-convention */

export interface RecentlyPlayedGamesParams {
  steamid: SteamID
}

export interface RecentGame {
  appid: AppID
  name: string
  playtime_2weeks: number
  playtime_forever: number
  playtime_linux_forever: number
  playtime_mac_forever: number
  playtime_windows_forever: number
  img_icon_url: string
  img_logo_url: string
}

interface RecentlyPlayedGames {
  response: {
    total_count: number
    games?: RecentGame[]
  }
}

/* eslint-disable @typescript-eslint/naming-convention */

export const getRecentlyPlayedGames = async (params: RecentlyPlayedGamesParams): Promise<RecentGame[]> => {
  const { response } = await fetch<RecentlyPlayedGames>('IPlayerService/GetRecentlyPlayedGames/v0001', params)
  return response.games || []
}
