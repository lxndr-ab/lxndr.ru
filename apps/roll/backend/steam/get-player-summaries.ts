import { fetch } from './request'

interface GetPlayerSummariesParams {
  steamids: string[]
}

enum VisibilityState {
  public = 3,
}

interface Player {
  steamid: string
  personaname: string
  profileurl: string
  avatar: string
  avatarmedium: string
  avatarfull: string
  personastate: number
  communityvisibilitystate: VisibilityState
  profilestate: number
  lastlogoff: number
  commentpermission: boolean
}

interface GetPlayerSummariesResult {
  response: {
    players: Player[]
  }
}

export const getPlayerSummaries = async ({ steamids, ...params }: GetPlayerSummariesParams): Promise<Player[]> => {
  const { response } = await fetch<GetPlayerSummariesResult>('ISteamUser/GetPlayerSummaries/v0002', {
    ...params,
    steamids: steamids.join(','),
  })

  return response.players
}
