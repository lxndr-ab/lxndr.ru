import { AxiosError } from 'axios'
import { FORBIDDEN } from 'http-status'
import { logger } from '../logger'
import { Language } from './general'
import { fetch } from './request'

export interface GetSchemaForGameParams {
  appid: number
  l?: Language
}

export interface Achievement {
  name: string
  defaultvalue: number
  displayName: string
  hidden: number
  description?: string
  icon: string
  icongray: string
}

export interface Game {
  gameName?: string
  gameVersion?: string
  availableGameStats?: {
    achievements?: Achievement[]
    stats?: [{
      name: string
      defaultvalue: number
      displayName: string
    }]
  }
}

export interface GetSchemaForGameResult {
  game: Game
}

export const getSchemaForGame = async (params: GetSchemaForGameParams): Promise<Game> => {
  try {
    const { game } = await fetch<GetSchemaForGameResult>('ISteamUserStats/GetSchemaForGame/v2', params)
    return game
  } catch (err) {
    if ('isAxiosError' in err) {
      const { response } = err as AxiosError

      /* Sometimes Steam returns 403 if not available in region */
      if (response?.status === FORBIDDEN && typeof response.data === 'object') {
        logger.warn(`GetUserStatsForGame for ${params.appid} returned 403`)
        return {}
      }

      throw err
    }

    throw err
  }
}
