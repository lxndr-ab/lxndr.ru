import ms from 'ms'
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import pThrottle from 'p-throttle'
import config from '../config'

// eslint-disable-next-line @typescript-eslint/ban-types
export type QueryParams = object

interface RequestConfig extends AxiosRequestConfig {
  params?: QueryParams
}

type Request = <T>(url: string, config?: RequestConfig) => Promise<AxiosResponse<T>>

const throttle = pThrottle({
  limit: config.steam.requestConcurrency,
  interval: ms('1d') / config.steam.requestLimit,
})

export const request: Request = throttle(axios.get.bind(axios))

export const formatQueryParams = (isService: boolean, params: QueryParams = {}): QueryParams => {
  const query = isService && Object.keys(params).length
    // eslint-disable-next-line @typescript-eslint/naming-convention
    ? { input_json: JSON.stringify(params) }
    : params

  return {
    key: config.steam.apiKeys[0],
    format: 'json',
    ...query,
  }
}

export const fetch = async <T>(
  method: string,
  params?: QueryParams,
): Promise<T> => {
  const isService = /^\w+Service\//u.test(method)

  const { data } = await request<T>(method, {
    baseURL: config.steam.apiUrl,
    params: formatQueryParams(isService, params),
  })

  return data
}
