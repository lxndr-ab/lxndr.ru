import { Platform } from '~/cm/types'
import { request } from './request'

export enum AppType {
  game = 'game',
}

export type Platforms = {
  [key in Platform]: boolean
}

export interface Category {
  id: number
  description: string
}

export interface Genre {
  id: string
  description: string
}

export interface AppDetails {
  type: AppType
  name: string
  // eslint-disable-next-line @typescript-eslint/naming-convention
  steam_appid: number
  platforms: Platforms
  // eslint-disable-next-line @typescript-eslint/naming-convention
  is_free: boolean
  categories?: Category[]
  genres?: Genre[]
}

export type GetAppDetailsResult = null | {
  [appid: number]: {
    success: boolean
    data?: AppDetails
  }
}

export interface GetAppDetailsParams {
  appids: number
}

export const getAppDetails = async (params: GetAppDetailsParams): Promise<AppDetails | null> => {
  const url = 'https://store.steampowered.com/api/appdetails'
  const { data } = await request<GetAppDetailsResult>(url, { params })

  if (data === null) {
    throw new Error()
  }

  const app = Object.values(data)[0]

  if (!app?.data) {
    throw new Error()
  }

  return app.data
}
