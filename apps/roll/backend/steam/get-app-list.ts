import { fetch } from './request'
import { Language } from './general'

/* eslint-disable @typescript-eslint/naming-convention */

interface AppListParams {
  if_modified_since?: number
  have_description_language?: Language
  include_games?: boolean
  include_dlc?: boolean
  include_software?: boolean
  include_videos?: boolean
  include_hardware?: boolean
}

export interface App {
  appid: number
  last_changed: number
  name: string
  price_change_number: number
}

interface AppListResult {
  response: {
    apps: App[]
    have_more_results?: boolean
    last_appid?: number
  }
}

/* eslint-enable @typescript-eslint/naming-convention */

export const getAppList = async (params: AppListParams = {}): Promise<App[]> => {
  const chunks: App[][] = []
  let lastAppID: number | undefined
  let haveMoreResults: boolean | undefined

  do {
    const normalizedParams = {
      ...params,
      last_appid: lastAppID,
      max_results: 50000,
    }

    const { response } = await fetch<AppListResult>('IStoreService/GetAppList/v1', normalizedParams)

    lastAppID = response.last_appid
    haveMoreResults = response.have_more_results

    chunks.push(response.apps)
  } while (haveMoreResults)

  return ([] as App[]).concat(...chunks)
}
