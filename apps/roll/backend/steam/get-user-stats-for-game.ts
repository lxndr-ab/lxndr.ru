import { Language } from './general'
import { fetch } from './request'

export interface GetUserStatsForGameParams {
  steamid: string
  appid: number
  l?: Language
}

interface UserAchievement {
  name: string
  achieved: 1
}

interface UserStat {
  name: string
  value: number
}

export interface GetUserStatsForGameResult {
  playerstats?: {
    steamID: string
    gameName: string
    achievements?: UserAchievement[]
    stats?: UserStat[]
  }
}

export interface UserStats {
  achievements: UserAchievement[]
  stats: UserStat[]
}

export const getUserStatsForGame = async (params: GetUserStatsForGameParams): Promise<UserStats> => {
  const { playerstats } = await fetch<GetUserStatsForGameResult>('ISteamUserStats/GetUserStatsForGame/v0002', params)
  const achievements = playerstats?.achievements || []
  const stats = playerstats?.stats || []
  return { achievements, stats }
}
