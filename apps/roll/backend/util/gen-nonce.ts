import { randomBytes } from 'crypto'

const NONCE_LEN = 8

export const genNonce = (): string =>
  randomBytes(NONCE_LEN).toString('hex')
