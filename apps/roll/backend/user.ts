import { SteamID } from '~/cm/types'
import config from './config'

export class User {
  constructor(public steamid: SteamID | null, public token: string | null) {}

  get authorized(): boolean {
    return !!this.steamid
  }

  get isAdmin(): boolean {
    return config.admin.steamid === this.steamid
  }
}
