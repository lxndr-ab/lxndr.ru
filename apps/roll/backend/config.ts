import Winston from 'winston'

const HOST = process.env.HOST || 'https://roll.lxndr.ru'

const config = {
  http: {
    port: 8443,
  },
  jwt: {
    secret: process.env.JWT_SECRET || '',
    issuer: process.env.JWT_ISSUER,
    sessionLife: '7 days',
  },
  admin: {
    steamid: process.env.ADMIN_STEAMID,
  },
  steam: {
    authReturnUrl: process.env.STEAM_WEBAPI_RETURN_URL || `${HOST}/auth/verify`,
    apiUrl: 'https://api.steampowered.com',
    apiKeys: [
      process.env.STEAM_WEBAPI_KEY || 'FAKEKEY',
    ],
    requestConcurrency: 1,

    /**
     * Steam limits requests to 100k per day.
     * Sometimes entire 100k can cause 429 error.
     * @default 100000
     */
    requestLimit: 95000,
  },
  db: {
    host: process.env.DB_HOST || 'http://localhost:5984',
    name: process.env.DB_NAME || 'roll',
    user: process.env.DB_USER || 'admin',
    pass: process.env.DB_PASS || '',
  },
  logger: {
    level: 'debug',
  },
}

/* Sanity check */
if (!config.jwt.secret) {
  throw new Error('JWT_SECRET not provided')
}

if (!config.jwt.issuer) {
  throw new Error('JWT_ISSUER not provided')
}

if (!config.steam.apiKeys[0]) {
  throw new Error('STEAM_WEBAPI_KEY not provided')
}

if (!config.db.pass) {
  throw new Error('DB_PASS not provided')
}

if (!config.admin.steamid) {
  throw new Error('ADMIN_STEAMID not provided')
}

export default config
