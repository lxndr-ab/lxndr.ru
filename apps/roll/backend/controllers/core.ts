import send from 'koa-send'
import { INTERNAL_SERVER_ERROR, OK } from 'http-status'
import { HttpError, Unauthorized } from 'http-errors'
import jwt from 'jsonwebtoken'
import ms from 'ms'
import { ValidationError } from 'yup'
import * as steamAuth from '~/be/steam/auth'
import { KoaContext, KoaMiddleware, TokenPayload } from '~/be/types'
import { User } from '~/be/user'
import * as db from '~/be/db'
import config from '~/be/config'
import { logger } from '~/be/logger'
import { genNonce } from '~/be/util'
import { updateUserGamesTask } from '~/be/services/steam-data'
import { isCouchError } from '~/be/db/client'
import { renderApp } from '~/fe/index.ssr'

const bearerRe = /Bearer: (.+)/iu

const getBearer = (ctx: KoaContext): string | null => {
  const value = ctx.get('Authorization')

  if (!value) {
    return null
  }

  const m = bearerRe.exec(value)
  return m ? m[1] : null
}

export const error: KoaMiddleware = async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    logger.error(err)

    ctx.body = err instanceof Error
      ? { message: err.message }
      : { message: 'Error' }
    ctx.status = INTERNAL_SERVER_ERROR

    if (err instanceof ValidationError) {
      ctx.status = 400
      ctx.body = { message: err.message }
    } else if (err instanceof HttpError) {
      ctx.status = err.statusCode
    } else if (isCouchError(err)) {
      ctx.body = { message: err.error }
    }
  }
}

type UserMiddleware = KoaMiddleware<{
  token?: string
}>

export const user: UserMiddleware = async (ctx, next) => {
  try {
    const token = getBearer(ctx) || ctx.cookies.get('token') || ctx.query.token

    if (!token) {
      throw new Unauthorized('No token was provided')
    }

    const options = { issuer: config.jwt.issuer }
    const { steamid } = jwt.verify(token, config.jwt.secret, options) as TokenPayload
    ctx.state.user = new User(steamid, token)
  } catch (err) {
    ctx.cookies.set('token', '')
    ctx.state.user = new User(null, null)
  }

  await next()
}

export const auth: KoaMiddleware = async (ctx, next) => {
  const { authorized } = ctx.state.user

  if (!authorized) {
    throw new Unauthorized()
  }

  await next()
}

export const authAdmin: KoaMiddleware = async (ctx, next) => {
  const { isAdmin } = ctx.state.user

  if (!isAdmin) {
    throw new Unauthorized()
  }

  await next()
}

export const login: KoaMiddleware = async ctx => {
  try {
    const authUrl = await steamAuth.authenticate()
    ctx.redirect(authUrl)
  } catch (err) {
    logger.error(err)
    throw new Unauthorized()
  }
}

export const verify: KoaMiddleware = async ctx => {
  try {
    const steamid = await steamAuth.verify(ctx.req)

    const payload = { steamid }
    const options = { issuer: config.jwt.issuer, expiresIn: config.jwt.sessionLife }
    const token = jwt.sign(payload, config.jwt.secret, options)

    ctx.cookies.set('token', token, { maxAge: ms(config.jwt.sessionLife), overwrite: true })
    ctx.redirect('/')
  } catch (err) {
    logger.error(err)
    throw new Unauthorized()
  }
}

export const logout: KoaMiddleware = ctx => {
  ctx.cookies.set('token', '')
  ctx.status = OK
}

export const bundle: KoaMiddleware = async ctx => {
  await send(ctx, ctx.path, { root: __dirname })
}

export const categories: KoaMiddleware = async ctx => {
  ctx.body = await db.getCategories()
}

export const genres: KoaMiddleware = async ctx => {
  ctx.body = await db.getGenres()
}

export const page: KoaMiddleware = async ctx => {
  const nonce = genNonce()

  const scps = [
    'default-src \'self\'',
    'frame-ancestors \'none\'',
    'font-src https://fonts.gstatic.com/',
    'img-src \'self\' https://cdn.cloudflare.steamstatic.com/steam/apps/',
    'style-src \'self\' \'unsafe-inline\'  https://fonts.googleapis.com/',
    'connect-src \'self\' https://www.google-analytics.com/g/collect',
    process.env.NODE_ENV === 'production'
      ? `script-src 'self' 'nonce-${nonce}' https://www.googletagmanager.com/gtag/js`
      : `script-src 'self' 'nonce-${nonce}' 'unsafe-eval' https://www.googletagmanager.com/gtag/js`,
  ]

  const scp = scps.map(v => `${v};`).join(' ')
  ctx.set('Content-Security-Policy', scp)

  const { user: { authorized, steamid, token } } = ctx.state

  if (authorized && steamid) {
    /* When user logs in, start updating their data. */
    void updateUserGamesTask(steamid)
  }

  const [categories, genres] = await Promise.all([
    db.getCategories(),
    db.getGenres(),
  ])

  ctx.status = OK
  ctx.type = 'html'
  ctx.body = await renderApp({ path: ctx.path, token, nonce, categories, genres })
}
