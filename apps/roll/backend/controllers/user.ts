import { OK } from 'http-status'
import { Unauthorized } from 'http-errors'
import * as yup from 'yup'
import qs from 'qs'
import { AppFilter, CompletionFilter, Platform, UserAppStatus } from '~/cm/types'
import * as db from '~/be/db'
import { KoaMiddleware } from '~/be/types'
import { randomArrayItem } from '~/be/util'

const markGameParamsSchema = yup.object({
  appid: yup.number().positive().min(0).required(),
})

const markGameQuerySchema = yup.object({
  status: yup.mixed<UserAppStatus>().oneOf(Object.values(UserAppStatus) as UserAppStatus[]).required(),
})

type MarkGameMiddleware = KoaMiddleware<{
  appid: string
}>

export const markGame: MarkGameMiddleware = async ctx => {
  const { steamid } = ctx.state.user

  if (!steamid) {
    throw new Unauthorized('Controller \'markGame\' can only be used by authorized user')
  }

  const { appid } = await markGameParamsSchema.validate(ctx.params)
  const { status } = await markGameQuerySchema.validate(ctx.request.body)
  await db.markGame(steamid, appid, status)
  ctx.status = OK
}

type UnmarkGameMiddleware = KoaMiddleware<{
  appid: string
}>

export const unmarkGame: UnmarkGameMiddleware = async ctx => {
  const { steamid } = ctx.state.user

  if (!steamid) {
    throw new Unauthorized('Controller \'unmarkGame\' can only be used by authorized user')
  }

  const { appid } = await markGameParamsSchema.validate(ctx.params)
  await db.unmarkGame(steamid, appid)
  ctx.status = OK
}

export const games: KoaMiddleware = async ctx => {
  const { steamid } = ctx.state.user

  if (!steamid) {
    throw new Unauthorized('Controller \'games\' can only be used by authorized user')
  }

  const apps = await db.getApps({ steamid })

  ctx.body = apps.map(({
    appid,
    name,
    completion,
    hasAchievements,
    userStatus,
  }) => ({
    appid,
    name,
    completion,
    hasAchievements,
    userStatus,
  }))
}

const pickRandomQueryScheme = yup.object({
  includeFree: yup.boolean().default(true),
  achievementsOnly: yup.boolean().default(false),
  platform: yup.mixed<Platform>().oneOf(Object.values(Platform) as Platform[]).default(Platform.windows),
  completion: yup.mixed<CompletionFilter>().oneOf(Object.values(CompletionFilter) as CompletionFilter[]).default(CompletionFilter.all),
  categories: yup.array(yup.number().required()).ensure(),
  genres: yup.array(yup.number().required()).ensure(),
})

type RandomMiddleware = KoaMiddleware<Record<keyof AppFilter, string | undefined>>

export const random: RandomMiddleware = async ctx => {
  const { steamid } = ctx.state.user

  if (!steamid) {
    throw new Unauthorized('Controller \'random\' can only be used by authorized user')
  }

  const query = qs.parse(ctx.search, { ignoreQueryPrefix: true })
  const normalizedQuery = await pickRandomQueryScheme.validate(query)
  const filter: AppFilter = { steamid, skipIgnored: true, ...normalizedQuery }
  const apps = await db.getApps(filter)
  const app = randomArrayItem(apps)

  if (!app) {
    ctx.body = null
    return
  }

  ctx.body = {
    appid: app.appid,
    name: app.name,
    hasAchievements: app.hasAchievements,
  }
}
