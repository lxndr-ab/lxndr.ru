function (doc, req) {
  const { data, meta } = JSON.parse(req.body)
  const _id = req.id
  const [type] = _id.split(':')
  
  const newDoc = {
    _id,
    type,
    updatedAt: Date.now(),
    data,
    meta,
  }
  
  if (doc) {
    newDoc._rev = doc._rev
  }
  
  return [newDoc, 'ok']
}
