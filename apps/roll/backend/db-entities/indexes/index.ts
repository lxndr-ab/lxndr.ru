/* eslint-disable @typescript-eslint/no-require-imports */

export default [
  require('./app-details.json'),
  require('./user-apps.json'),
  require('./categories.json'),
  require('./genres.json'),
  require('./needs-update.json'),
]
