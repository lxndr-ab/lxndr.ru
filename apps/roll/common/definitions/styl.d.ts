declare module '*.styl' {
  type Classes = Record<string, string>
  const classes: Classes
  export = classes
}
