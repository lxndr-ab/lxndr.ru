export enum Platform {
  windows = 'windows',
  mac = 'mac',
  linux = 'linux',
}

export enum CompletionFilter {
  all = 'all',
  neverPlayed = 'neverPlayed',
  played = 'played',
  unfinished = 'unfinished',
  uncompleted = 'uncompleted',
}

export type SteamID = string
export type AppID = number

export interface AppFilter {
  steamid: SteamID
  includeFree?: boolean
  achievementsOnly?: boolean
  platform?: Platform
  skipIgnored?: boolean
  completion?: CompletionFilter
  categories?: number[]
  genres?: number[]
}

export enum Completion {
  neverPlayed = 'neverPlayed',
  played = 'played',
  finished = 'finished',
  completed = 'completed',
}

export enum UserAppStatus {
  ignored = 'ignored',
  finished = 'finished',
  completed = 'completed',
}

export interface UserApp {
  status: UserAppStatus
}

export interface Achievement {
  name: string
  achieved: boolean
  finish: boolean
}

export interface Genre {
  id: number
  name: string
}

export interface Category {
  id: number
  name: string
}

export interface App {
  appid: AppID
  name: string
  isFree: boolean
  completion: Completion
  platforms: Platform[]
  hasAchievements: boolean
  achievements: Record<string, Achievement>
  genres: number[]
  categories: number[]
  userStatus?: UserAppStatus
}
