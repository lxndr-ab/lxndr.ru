import { useQuery } from 'react-query'
import { App, UserAppStatus } from '~/cm/types'
import { useUser } from '~/fe/providers/user'
import * as api from './api'

export const PICKED_GAME_QUERY_KEY = 'pickedGame'

interface UseGamePickerOptions {
  getGameFilter: () => api.FilterFields
}

export const useGamePicker = ({ getGameFilter }: UseGamePickerOptions) => {
  const { token } = useUser()

  const { data: pickedGame, refetch: pickRandomGame, isLoading, isFetched, error } =
    useQuery<App, Error>({
      queryKey: PICKED_GAME_QUERY_KEY,
      queryFn: () => api.fetchRandomGame(getGameFilter(), token),
      enabled: false,
      refetchIntervalInBackground: false,
      refetchOnMount: false,
      refetchOnWindowFocus: false,
      refetchOnReconnect: false,
    })

  const markGame = async (appid: number, status: UserAppStatus) => {
    await api.markGame(appid, status, token)
    pickRandomGame()
  }

  return {
    pickedGame,
    pickRandomGame() {
      pickRandomGame()
    },
    isLoading,
    isFetched,
    error,
    markGame,
  }
}
