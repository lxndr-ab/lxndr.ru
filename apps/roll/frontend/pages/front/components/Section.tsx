import { FC, ReactNode } from 'react'
import styled from '@emotion/styled'

interface SectionProps {
  label?: string
  children: ReactNode
}

export const Section: FC<SectionProps> = ({ label, children }) =>
  <Container>
    {!!label && <Label>{ label }</Label>}
    { children }
  </Container>

const Container = styled.div`
  text-align: left;
  margin: 10px 0;
`

const Label = styled.div`
  font-weight: bold;
  text-align: left;
  margin: 5px 0;
`
