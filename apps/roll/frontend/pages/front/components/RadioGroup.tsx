import styled from '@emotion/styled'
import { FC } from 'react'
import { Radio } from '~/fe/components/Radio'

export interface RadioOption {
  value: string
  label: string
}

interface RadioGroupProps {
  options: RadioOption[]
  name?: string
  value?: string
  onChange?: (value: string) => void
}

export const RadioGroup: FC<RadioGroupProps> = ({ options, name, value, onChange }) =>
  <Container>
    {options.map(option =>
      <StyledRadio
        key={option.value}
        name={name}
        label={option.label}
        value={option.value}
        checked={option.value === value}
        onChange={({ target }) => target.checked && onChange?.(option.value)}
      />
    )}
  </Container>

const Container = styled.div`
  margin: 0 5px;
`

const StyledRadio = styled(Radio)`
  display: inline-block;
  width: 130px;
`
