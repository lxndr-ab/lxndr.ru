import { FC } from 'react'
import styled from '@emotion/styled'

interface Item {
  id: number
  name: string
}

interface MultiselectProps {
  items: Item[]
  value?: number[]
  onChange?: (value: number[]) => void
}

const sortByName = (items: Item[]) =>
  items.slice().sort((a, b) => a.name.localeCompare(b.name))

export const Multiselect: FC<MultiselectProps> = ({
  items,
  value = [],
  onChange = () => {},
}) => {
  const handleClick = (id: number) => {
    const index = value.indexOf(id)

    if (index === -1) {
      const list = value.slice()
      list.push(id)
      onChange(list)
    } else {
      const list = value.slice()
      list.splice(index, 1)
      onChange(list)
    }
  }

  const handleDeselectAll = () => {
    onChange([])
  }

  const sortItems = sortByName(items)

  return (
    <Container>
      <DeselectAllButton
        key="none"
        type="button"
        onClick={handleDeselectAll}
      >
        Deselect All
      </DeselectAllButton>

      {sortItems.map(item => (
        <ItemButton
          key={item.id}
          type="button"
          selected={value.includes(item.id)}
          onClick={() => handleClick(item.id)}
        >
          {item.name}
        </ItemButton>
      ))}
    </Container>
  )
}

const Container = styled.div`
  text-align: left;
  padding: 0;
`

const ItemButton = styled.button<{ selected?: boolean }>`
  display: inline-block;
  padding: 5px 10px;
  margin: 5px;
  background: ${({ selected = false }) =>
    (selected ? 'lightgreen' : 'lightblue')};
  border: none;
  border-radius: 2px;
`

const DeselectAllButton = styled(ItemButton)`
  background: orange;
`
