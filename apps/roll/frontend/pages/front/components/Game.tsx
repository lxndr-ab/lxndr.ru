import { FC } from 'react'
import styled from '@emotion/styled'
import { App } from '~/cm/types'
import { SteamAppLogo } from '~/fe/components/SteamAppLogo'

export const Game: FC<App> = ({ appid, name }) =>
  <Container>
    <div>
      <SteamAppLogo appid={appid} />
    </div>

    <Name>{ name }</Name>

    <div>
      <Action
        href={`https://store.steampowered.com/app/${appid}/`}
        target="_blank"
        rel="noreferrer"
      >
        Open in store
      </Action>

      <Action
        href={`steam://run/${appid}`}
      >
        Launch
      </Action>
    </div>
  </Container>

const Container = styled.div`
  display: inline-block;
  text-align: center;
  width: 250px;
`

const Name = styled.div`
  height: 40px;
  padding: 5px;
`

const Action = styled.a`
  padding: 10px;
`
