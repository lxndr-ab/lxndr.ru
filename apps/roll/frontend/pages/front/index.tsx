import { FC, useEffect } from 'react'
import { useForm } from 'rc-field-form'
import styled from '@emotion/styled'
import { UserAppStatus } from '~/cm/types'
import { useFilterStorage } from './useFilterStorage'
import { FilterFields } from './api'
import { RandomGameForm } from './components/RandomGameForm'
import { NoGame } from './components/NoGame'
import { Game } from './components/Game'
import { useGamePicker } from './useGamePicker'

export const FrontPage: FC = () => {
  const { storedFilter, storeFilter } = useFilterStorage()
  const [filterForm] = useForm<FilterFields>()

  const {
    pickedGame,
    pickRandomGame,
    markGame,
    isLoading,
    isFetched,
    error,
  } = useGamePicker({
    getGameFilter: () => filterForm.getFieldsValue(),
  })

  useEffect(() => {
    filterForm.setFieldsValue(storedFilter)
  }, [])

  const handleSpinClick = () => {
    storeFilter(filterForm.getFieldsValue())
    pickRandomGame()
  }

  const handleResetClick = () => {
    filterForm.resetFields()
    pickRandomGame()
  }

  const renderStatus = () => {
    if (isLoading) {
      return <div>Loading...</div>
    }

    if (error) {
      return <div>{error.message}</div>
    }

    if (pickedGame) {
      return (
        <div>
          <Game {...pickedGame} />

          <MarkGameWrapper>
            <button type="button" onClick={() => markGame(pickedGame.appid, UserAppStatus.ignored)}>
              Ignore this game
            </button>
            <button type="button" onClick={() => markGame(pickedGame.appid, UserAppStatus.finished)}>
              I finished this game
            </button>
            {!pickedGame.hasAchievements &&
              <button type="button" onClick={() => markGame(pickedGame.appid, UserAppStatus.completed)}>
                I completed this game
              </button>}
          </MarkGameWrapper>
        </div>
      )
    }

    if (isFetched) {
      return <NoGame onResetClick={handleResetClick} />
    }

    return (
      <Note>
        Note that it might take some time for the server to fetch information about your games.
        <br />
        Also, your steam game list has to be publicly available.
      </Note>
    )
  }

  return (
    <>
      <RandomGameForm form={filterForm} onSpinClick={handleSpinClick} />

      <GameContainer>
        {renderStatus()}
      </GameContainer>
    </>
  )
}

const MarkGameWrapper = styled.div`
  margin-top: 20px;
`

const GameContainer = styled.div`
  margin-top: 20px;
`

const Note = styled.div`
  color: gray;
  margin-top: 8px;
  font-style: italic;
`
