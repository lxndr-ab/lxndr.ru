import { useQuery } from 'react-query'
import { useUser } from '~/fe/providers/user'
import { fetchGameList } from './api'

export const GAME_LIST_QUERY_KEY = 'gameList'

export const useGameList = () => {
  const { token } = useUser()

  const { data: gameList } = useQuery({
    queryKey: GAME_LIST_QUERY_KEY,
    queryFn: () => fetchGameList(token),
  })

  return {
    gameList
  }
}
