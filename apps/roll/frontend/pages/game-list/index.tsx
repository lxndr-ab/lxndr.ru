import { FC } from 'react'
import styled from '@emotion/styled'
import { Completion, UserAppStatus } from '~/cm/types'
import { SteamAppLogo } from '~/fe/components/SteamAppLogo'
import { useGameList } from './useGameList'

const Container = styled.table`
  margin: 0 auto;
  text-align: left;
  border-spacing: 3px;
`

const StatusCell = styled.td`
  white-space: nowrap;
  text-align: right;
`

const userAppStatusMap = {
  [UserAppStatus.ignored]: 'Ignored',
  [UserAppStatus.finished]: 'Finished',
  [UserAppStatus.completed]: 'Completed',
}

const completionMap = {
  [Completion.neverPlayed]: '',
  [Completion.played]: 'Played',
  [Completion.finished]: 'Finished',
  [Completion.completed]: 'Completed',
}

export const GameListPage: FC = () => {
  const { gameList } = useGameList()

  if (!gameList) {
    return <div>Loading...</div>
  }

  return (
    <Container>
      <colgroup>
        <col />
        <col />
        <col />
        <col />
        <col />
      </colgroup>
      <tbody>
        {gameList.map(game => (
          <tr key={game.appid}>
            <td><SteamAppLogo appid={game.appid} /></td>
            <td>{ game.appid }</td>
            <td>{ game.name }</td>
            <StatusCell>{ game.userStatus && userAppStatusMap[game.userStatus] }</StatusCell>
            <StatusCell>{ completionMap[game.completion] }</StatusCell>
          </tr>
        ))}
      </tbody>
    </Container>
  )
}
