import { FC } from 'react'
import styled from '@emotion/styled'

const Container = styled.div`
  text-align: center;
  margin-top: 20px;
`

const Paragraph = styled.p``

const LoginLink = styled.a``

export const LoginPage: FC = () =>
  <Container>
    <Paragraph>
      Welcome!
    </Paragraph>

    <Paragraph>
      This is Steam Game Roulette that allows you randomly choose a game from your Steam library.
    </Paragraph>

    <Paragraph>
      To proceed, you have to log in into your Steam account.
      Don&apos;t worry, all it does is telling what your SteamID is.
    </Paragraph>

    <Paragraph>
      You also have to have your Steam account publicly open
      so that we could fetch the list of your games and achievements.
    </Paragraph>

    <LoginLink href="/auth/login">
      Sign in via Steam
    </LoginLink>
  </Container>
