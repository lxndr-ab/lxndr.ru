import { FC, useState } from 'react'
import styled from '@emotion/styled'
import { Achievement } from '~/be/steam/get-schema-for-game'
import { Button } from '~/fe/components/Button'
import { AchievementAutosuggest } from './AchievementAutosuggest'

const ADD_ICON = '+'
const REMOVE_ICON = '\uD83D\uDDD1'

export interface AchievementListProps {
  availableAchievements: Achievement[]
  value?: string[]
  onChange?: (ids: string[]) => void
}

export const AchievementList: FC<AchievementListProps> = ({
  availableAchievements,
  value: selectedIds = [],
  onChange = () => {},
}) => {
  const [inputValue, setInputValue] = useState('')

  const selectedAchievements = selectedIds.map((id: string): Achievement => {
    const achievement = availableAchievements.find(it => it.name === id)
    return achievement || { name: id, displayName: id, hidden: 0, icon: '', icongray: '', defaultvalue: 0 }
  })

  const handleRemoveClick = (id: string) =>
    onChange(selectedIds.filter(it => it !== id))

  const handleAddClick = () => {
    if (inputValue) {
      onChange([...selectedIds, inputValue])
      setInputValue('')
    }
  }

  return (
    <Container>
      <List>
        {selectedAchievements.map(it => (
          <Row key={it.name}>
            <Item key={it.name}>
              <Title>{it.displayName}</Title>
              <Description>{it.description}</Description>
            </Item>
            <RemoveButton type="button" onClick={() => handleRemoveClick(it.name)}>
              {REMOVE_ICON}
            </RemoveButton>
          </Row>
        ))}
      </List>
      <Row>
        <AchievementAutosuggest
          availableAchievements={availableAchievements}
          value={inputValue}
          onChange={setInputValue}
        />
        <AddButton type="button" onClick={handleAddClick}>
          {ADD_ICON}
        </AddButton>
      </Row>
    </Container>
  )
}

const Container = styled.div`
  min-width: 300px;
`

const List = styled.div``

const Row = styled.div`
  display: flex;
  flex-direction: row;
  margin: 1px 0;
`

const Item = styled.div`
  background: lightblue;
  padding: 5px 10px;
  width: 100%;

  &:first-of-type {
    border-radius-top-right: 3px;
    border-radius-top-left: 3px;
  }

  &:last-of-type {
    border-radius-bottom-right: 3px;
    border-radius-bottom-left: 3px;
  }
`

const Title = styled.div`
  font-weight: bold;
`

const Description = styled.div`
`

const RemoveButton = styled(Button)`
  width: 20px;
  height: 20px;
  padding: 0;
  margin-left: 5px;
  background: red;
  font-weight: bold;
  flex: 1 0 auto;
`

const AddButton = styled(RemoveButton)`
  background: green;
`
