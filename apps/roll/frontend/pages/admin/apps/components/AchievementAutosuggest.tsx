import { FC, useState } from 'react'
import Autosuggest, {
  InputProps,
  OnSuggestionsClearRequested,
  SuggestionsFetchRequested,
} from 'react-autosuggest'
import styled from '@emotion/styled'
import { Achievement } from '~/be/steam/get-schema-for-game'

interface AchievementAutosuggestProps {
  availableAchievements: Achievement[]
  value: string
  onChange: (value: string) => void
}

const renderSuggestion = ({ displayName, description }: Achievement) => (
  <div>
    <div>{displayName}</div>
    <div>{description}</div>
  </div>
)

export const AchievementAutosuggest: FC<AchievementAutosuggestProps> = ({ availableAchievements, value, onChange }) => {
  const [suggestions, setSuggestions] = useState<Achievement[]>([])

  const handleChange: InputProps<Achievement>['onChange'] = (event, { newValue }) => onChange(newValue)

  const handleSuggestionsFetchRequested: SuggestionsFetchRequested = ({ value }) => {
    const list = availableAchievements.filter(achievement => {
      const name = achievement.name.toLocaleLowerCase()
      const displayName = achievement.displayName.toLocaleLowerCase()
      const description = achievement.description?.toLocaleLowerCase()
      const val = value.toLocaleLowerCase()
      return name.includes(val) || displayName.includes(val) || description?.includes(val)
    })

    setSuggestions(list)
  }

  const handleSuggestionsClearRequested: OnSuggestionsClearRequested = () => setSuggestions([])

  return (
    <Container>
      <Autosuggest<Achievement>
        suggestions={suggestions}
        renderSuggestion={renderSuggestion}
        getSuggestionValue={suggestion => suggestion.name}
        inputProps={{ onChange: handleChange, value }}
        onSuggestionsFetchRequested={handleSuggestionsFetchRequested}
        onSuggestionsClearRequested={handleSuggestionsClearRequested}
      />
    </Container>
  )
}

const Container = styled.div`
  width: 100%;

  .react-autosuggest__container {
    width: 100%;
  }

  .react-autosuggest__input {
    width: 100%;
  }
`
