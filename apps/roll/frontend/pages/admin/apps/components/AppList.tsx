import { FC, useRef } from 'react'
import { useVirtual } from '@tanstack/react-virtual'
import styled from '@emotion/styled'
import { OwnedGame } from '~/be/steam'
import { AppRow } from './AppRow'

interface AppListProps {
  apps: OwnedGame[]
}

export const AppList: FC<AppListProps> = ({ apps }) => {
  const parentRef = useRef<HTMLDivElement>(null)

  const { totalSize, virtualItems } = useVirtual({
    size: apps.length,
    parentRef,
  })

  return (
    <Container ref={parentRef}>
      <div
        style={{
          height: totalSize,
          position: 'relative',
        }}
      >
        {virtualItems.map(({ index, measureRef, start }) => {
          const app = apps[index]
          const appid = app.appid

          return (
            <AppRow
              key={appid}
              ref={measureRef}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                transform: `translateY(${start}px)`,
              }}
              app={app}
            />
          )
        })}
      </div>
    </Container>
  )
}

const Container = styled.div`
  flex-grow: 1;
  overflow: auto;
`
