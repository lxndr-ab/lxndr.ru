import { useQuery } from 'react-query'
import { sortBy, uniqBy, prop } from 'ramda'
import { useUser } from '~/fe/providers/user'
import { fetchUserGames } from './api'
import { OwnedGame } from '~/be/steam'

interface UseAppsOptions {
  filter: string
}

const uniqByAppID = uniqBy<OwnedGame, unknown>(prop('appid'))
const sortByName = sortBy<OwnedGame>(prop('name'))

export const useApps = ({ filter }: UseAppsOptions) => {
  const { token } = useUser()

  const { data, isLoading } = useQuery({
    queryKey: 'admin:steam.user.games',
    queryFn: () => fetchUserGames(token),
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
  })

  const lowercaseFilter = filter.toLowerCase()

  const ownedGames = data?.flatMap(game => game.data) || []
  const uniqGames = uniqByAppID(ownedGames)

  const filteredApps = filter
    ? uniqGames.filter(game => game.name.toLowerCase().includes(lowercaseFilter))
    : uniqGames

  const sortedApps = sortByName(filteredApps)

  return {
    isLoading,
    apps: sortedApps,
  }
}
