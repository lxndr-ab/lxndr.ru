import { AppID } from '~/cm/types'
import { AppInfo, AppInfoEntity } from '~/be/db/types'
import { AppDetailsEntity } from '~/be/services/steam-data/update-app-details'
import { AppSchemaEntity } from '~/be/services/steam-data/update-app-schema'
import { UserGamesEntity } from '~/be/services/steam-data/update-user-games'
import { apiRequest } from '~/fe/util/apiRequest'

export const fetchAppDetails = async (appid: AppID, token?: string) =>
  await apiRequest<AppDetailsEntity>({
    url: `/rest/steam.app.details:${appid}`,
    token,
  })

export const fetchAppSchemas = async (appid: AppID, token?: string) =>
  await apiRequest<AppSchemaEntity>({
    url: `/rest/steam.app.schema:${appid}`,
    token,
  })

export const fetchUserGames = async (token?: string) =>
  await apiRequest<UserGamesEntity[]>({
    url: '/rest/steam.user.games',
    token,
  })

export const fetchAppInfo = async (appid: AppID, token?: string) =>
  await apiRequest<AppInfoEntity>({
    url: `/rest/app.info:${appid}`,
    token,
  })

export const updateAppInfoRequest = async (appid: AppID, appInfo: AppInfo, token?: string) =>
  await apiRequest({
    url: `/rest/app.info:${appid}`,
    method: 'PUT',
    data: appInfo,
    token,
  })
