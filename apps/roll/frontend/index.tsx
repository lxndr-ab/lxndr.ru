import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { QueryClient } from 'react-query'
import { App } from '~/fe/components/App'

const dehydratedState = window.__REACT_QUERY_STATE__

const queryClient = new QueryClient()

const app =
  <BrowserRouter>
    <App queryClient={queryClient} dehydratedState={dehydratedState} />
  </BrowserRouter>

hydrate(app, document.getElementById('app'))
