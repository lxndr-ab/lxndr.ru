import { FC, InputHTMLAttributes, DetailedHTMLProps } from 'react'
import styled from '@emotion/styled'

interface RadioProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  label: string
}

export const Radio: FC<RadioProps> = ({
  className,
  label,
  ...props
}) =>
  <Label className={className}>
    <Input type="radio" {...props} />
    <Text>{label}</Text>
  </Label>

const Label = styled.label`
  line-height: 18px;
`

const Input = styled.input`
  margin: 0;
  vertical-align: middle;
`

const Text = styled.span`
  margin-left: 8px;
  vertical-align: middle;
`
