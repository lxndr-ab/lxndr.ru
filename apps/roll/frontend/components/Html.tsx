import { FC, ReactNode } from 'react'

interface HtmlProps {
  title?: string
  jsBundle?: string
  dehydratedState: unknown
  nonce: string
  children: ReactNode
}

const formatIntialState = (state: unknown) =>
  `window.__REACT_QUERY_STATE__ = ${JSON.stringify(state)}`

const gaScript = `
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-LJT2DE4R12');
`

export const Html: FC<HtmlProps> = ({
  title = 'Steam Game Roulette',
  jsBundle = '/bundle.js',
  dehydratedState,
  nonce,
  children,
}) => (
  <html lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>{title}</title>
      <link href="https://fonts.googleapis.com/css?family=Exo+2:400,700&display=swap" rel="stylesheet" />

      <script async src="https://www.googletagmanager.com/gtag/js?id=G-LJT2DE4R12" />
      <script nonce={nonce} dangerouslySetInnerHTML={{ __html: gaScript }} />
    </head>

    <body>
      <div id="app">{children}</div>

      <script
        type="text/javascript"
        nonce={nonce}
        dangerouslySetInnerHTML={{ __html: formatIntialState(dehydratedState) }}
      />

      <script
        type="text/javascript"
        src={jsBundle}
      />
    </body>
  </html>
)
