import styled from '@emotion/styled'

export const Button = styled.button`
  border: none;
  border-radius: 2px;
  padding: 4px 2px;
  cursor: pointer;
`
