import styled from '@emotion/styled'

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  box-sizing: border-box;

  &:not(:first-of-type) {
    margin-top: 8px;
  }
`
