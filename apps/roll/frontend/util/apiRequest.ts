import axios, { AxiosRequestConfig } from 'axios'

interface ApiRequestConfig<D = unknown> extends AxiosRequestConfig<D> {
  token?: string
}

export const apiRequest = async <T>(config: ApiRequestConfig) => {
  const { token } = config

  const headers = token
    ? { ...config.headers, 'Authorization': `Bearer: ${token}` }
    : config.headers

  const res = await axios({
    ...config,
    baseURL: '/api',
    headers,
  })

  return res.data as T
}
