import { renderToNodeStream } from 'react-dom/server'
import { dehydrate, QueryClient } from 'react-query'
import { MemoryRouter } from 'react-router'
import createCache from '@emotion/cache'
import createEmotionServer from '@emotion/server/create-instance'
import { Category, Genre } from '~/cm/types'
import { Html } from '~/fe/components/Html'
import { App } from '~/fe/components/App'
import { USER_QUERY_KEY } from '~/fe/providers/user'
import { CATEGORIES_QUERY_KEY, GENRES_QUERY_KEY } from '~/fe/query-keys'

interface CreateAppArgs {
  path: string
  token: string | null
  nonce: string
  categories: Category[]
  genres: Genre[]
}

const emotionCache = createCache({ key: 'custom' })
const emotionServer = createEmotionServer(emotionCache)

export const renderApp = async ({ path, token, nonce, categories, genres }: CreateAppArgs) => {
  const queryClient = new QueryClient()

  if (token) {
    queryClient.setQueryData(USER_QUERY_KEY, token)
    queryClient.setQueryData(CATEGORIES_QUERY_KEY, categories)
    queryClient.setQueryData(GENRES_QUERY_KEY, genres)
  }

  const dehydratedState = dehydrate(queryClient)

  const app =
    <Html dehydratedState={dehydratedState} nonce={nonce}>
      <MemoryRouter initialEntries={[path]}>
        <App queryClient={queryClient} dehydratedState={dehydratedState} />
      </MemoryRouter>
    </Html>

  return renderToNodeStream(app).pipe(emotionServer.renderStylesToNodeStream())
}
