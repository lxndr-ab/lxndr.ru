import { FC } from 'react'
import { Category } from '@prisma/client'

interface CategorySelectProps {
  categories: Category[]
  value: string
  onChange: (value: string) => void
}

export const CategorySelect: FC<CategorySelectProps> = ({ categories, value, onChange }) =>
  <select value={value} onChange={({ target }) => onChange(target.value)}>
    <option key="any" value="">Any</option>

    {categories.map(category => (
      <option
        key={category.id}
        value={category.id}
      >
        {category.title}
      </option>
    ))}
  </select>
