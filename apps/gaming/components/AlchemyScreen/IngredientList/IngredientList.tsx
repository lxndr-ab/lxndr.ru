import { Item } from '@prisma/client'
import { FC } from 'react'
import { IngredientItem } from './IngredientItem'
import styles from './IngredientList.module.sass'

interface IngredientListProps {
  items: Item[]
}

export const IngredientList: FC<IngredientListProps> = ({ items }) => (
  <ul className={styles.list}>
    {items.map(item => (
      <IngredientItem
        key={item.id}
        item={item}
      />
    ))}
  </ul>
)
