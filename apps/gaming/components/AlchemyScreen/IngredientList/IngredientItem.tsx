import { Item } from '@prisma/client'
import { FC } from 'react'
import Image from 'next/image'
import { isBaseMod, useGameData } from '../../../providers/GameDataProvider'
import styles from './IngredientItem.module.sass'

interface IngredientItemProps {
  item: Item
}

export const IngredientItem: FC<IngredientItemProps> = ({ item }) => {
  const { findModById } = useGameData()
  const mod = findModById(item.modId)

  return (
    <li className={styles.item}>
      <div className={styles.iconWrapper}>
        {item.iconFileHash &&
          <Image
            width={24}
            height={24}
            objectFit="contain"
            src={`${process.env.NEXT_PUBLIC_HOST}/api/files/${item.iconFileHash}`}
            alt={item.title}
          />
        }
      </div>
      <div className={styles.name}>{item.title}</div>
      <div className={styles.info}>
        {item.tag === 'quest' && (
          <span className={styles.questTag}>Q</span>
        )}
        {!isBaseMod(mod) && (
          <span className={styles.modAbbr}>{mod.abbr}</span>
        )}
        {!!item.price && (
          <div className={styles.price}>
            <span className={styles.priceIcon} />
            <span className={styles.priceLabel}>{item.price.toString()}</span>
          </div>
        )}
        {!!item.weight && (
          <div className={styles.weight}>
            <span className={styles.weightIcon} />
            <span className={styles.weightLabel}>{item.weight}</span>
          </div>
        )}
      </div>
    </li>
  )
}
