import { FC } from 'react'
import { useRouter } from 'next/router'
import { EffectList } from './EffectList/EffectList'
import { IngredientList } from './IngredientList/IngredientList'
import { normalizeQuery } from '../../utils/normalizeQuery'
import { useGameData } from '../../providers/GameDataProvider'
import styles from './AlchemyScreen.module.sass'

export const AlchemyScreen: FC = () => {
  const router = useRouter()
  const query = normalizeQuery(router.query)
  const { effects, filterItems } = useGameData()
  const ingredients = filterItems({ effect: query.effect })

  return (
    <div className={styles.container}>
      <div className={styles.effectPane}>
        <EffectList
          effects={effects}
          selected={query.effect}
        />
      </div>
      <div className={styles.ingredientPane}>
        <IngredientList
          items={ingredients}
        />
      </div>
    </div>
  )
}
