import { Effect } from '@prisma/client'
import { FC } from 'react'
import { EffectItem } from './EffectItem'
import styles from './EffectList.module.sass'

interface EffectListProps {
  effects: Effect[]
  selected: string | null
}

export const EffectList: FC<EffectListProps> = ({ effects, selected = null }) => (
  <ul className={styles.list}>
    {effects.map((effect) => (
      <EffectItem
        key={effect.id}
        effect={effect}
        selected={effect.id === selected}
      />
    ))}
  </ul>
)
