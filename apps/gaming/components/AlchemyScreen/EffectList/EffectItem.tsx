import { FC } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import cn from 'classnames'
import { Effect } from '@prisma/client'
import { useRelativeLocation } from '../../../utils/useRelativeLocation'
import styles from './EffectItem.module.sass'

interface EffectItemProps {
  effect: Effect
  selected: boolean
}

export const EffectItem: FC<EffectItemProps> = ({ effect, selected }) => {
  const { calcRelRoute } = useRelativeLocation()

  return (
    <li className={cn(styles.item, { selected })}>
      <Link className={styles.link} href={calcRelRoute({ effect: effect.id })}>
        <span className={styles.iconWrapper}>
          {effect.iconFileHash &&
            <Image
              width={24}
              height={24}
              objectFit="contain"
              src={`${process.env.NEXT_PUBLIC_HOST}/api/files/${effect.iconFileHash}`}
              alt={effect.title}
            />
          }
        </span>
        <span className={styles.name}>{effect.title}</span>
      </Link>
    </li>
  )
}
