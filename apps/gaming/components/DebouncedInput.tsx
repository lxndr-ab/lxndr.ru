import { useState, useMemo, useEffect, FC, ChangeEventHandler } from 'react'
import debounce from 'lodash/debounce'

interface DebouncedInputProps {
  value: string
  onChange: (val: string) => void
}

export const DebouncedInput: FC<DebouncedInputProps> = ({ value, onChange, ...props }) => {
  const [currentValue, setCurrentValue] = useState(value)

  useEffect(
    () => setCurrentValue(value),
    [value],
  )

  const debouncer = useMemo(
    () => debounce(onChange, 200),
    [onChange],
  )

  const handleChange: ChangeEventHandler<HTMLInputElement> = ({ target: { value } }) => {
    setCurrentValue(value);
    debouncer(value);
  }

  return <input {...props} value={currentValue} onChange={handleChange} />
}
