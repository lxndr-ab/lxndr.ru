import { FC } from 'react'
import Link from 'next/link'
import { useGameData } from '../providers/GameDataProvider'
import ModList from './ModList'
import styles from './Header.module.sass'

const Header: FC = () => {
  const { game } = useGameData()

  return (
    <nav className={styles.header}>
      <Link href="/">To the list</Link>
      <div className={styles.gameName}>{game.title}</div>
      <ModList />
    </nav>
  )
}

export default Header
