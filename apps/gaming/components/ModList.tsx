import { FC } from 'react'
import { Mod } from '@prisma/client'
import without from 'lodash/without'
import union from 'lodash/union'
import Link from 'next/link'
import cn from 'classnames'
import { useGameData } from '../providers/GameDataProvider'
import { useRelativeLocation } from '../utils/useRelativeLocation'
import styles from './ModList.module.sass'

interface ModButtonProps {
  mod: Mod
}

const ModButton: FC<ModButtonProps> = ({ mod }) => {
  const { calcRelRoute } = useRelativeLocation()
  const { availableMods, selectedMods } = useGameData()

  const selectedIds = selectedMods.map(mod => mod.id)
  const checked = selectedIds.includes(mod.id)

  const newlySelectedIds = checked
    ? without(selectedIds, mod.id)
    : union(selectedIds, [mod.id])

  const mods = newlySelectedIds.length === 0
    ? 'none'
    : availableMods.length === newlySelectedIds.length
      ? undefined
      : newlySelectedIds.join(',')

  return (
    <Link className={cn(styles.modButton, { [styles.checked]: checked })} href={calcRelRoute({ mods })}>
      {mod.title}
    </Link>
  )
}

const ModList: FC = () => {
  const { availableMods } = useGameData()

  return (
    <div className={styles.modList}>
      {availableMods.map(mod => <ModButton key={mod.id} mod={mod} />)}
    </div>
  )
}

export default ModList
