import { FC, useMemo } from 'react'
import { useRouter } from 'next/router'
import { useRelativeLocation } from '../../utils/useRelativeLocation'
import { normalizeQuery } from '../../utils/normalizeQuery'
import { useGameData } from '../../providers/GameDataProvider'
import { Filter, FilterBar } from './FilterBar'
import { Row } from './Row'
import styles from './CraftingScreen.module.sass'

export const CraftingScreen: FC = () => {
  const router = useRouter()
  const { pushRelRoute } = useRelativeLocation<Filter>()
  const { effects, categories, filterRecipes } = useGameData()
  const { category = '', search = '' } = normalizeQuery(router.query)

  const recipes = useMemo(
    () =>
      filterRecipes({
        category,
        search,
      }),
    [category, search, filterRecipes],
  )

  const columns = useMemo(() => ({
    effects: !!effects.length,
    requirements: recipes.some(recipe => recipe.requirements.length),
    category: !!categories.length,
  }), [recipes, effects.length, categories.length])

  const handleFilterChange = (filter: Filter) => pushRelRoute(filter)

  return (
    <div>
      <FilterBar
        categories={categories}
        filter={{ category, search }}
        onChange={handleFilterChange}
      />

      <table className={styles.itemList} id="item-list">
        <thead className={styles.header}>
          <tr>
            <th>Name</th>
            {columns.effects && <th>Effects</th>}
            <th>Components</th>
            {columns.requirements && <th>Requirements</th>}
            {columns.category && <th>Category</th>}
          </tr>
        </thead>
        <tbody>
          {recipes.map(recipe => (
            <Row key={recipe.id} recipe={recipe} columns={columns} />
          ))}
        </tbody>
      </table>
    </div>
  )
}
