import { FC } from 'react'
import { Category } from '@prisma/client'
import { CategorySelect } from '../CategorySelect'
import { DebouncedInput } from '../DebouncedInput'
import styles from './FilterBar.module.sass'

export interface Filter {
  category: string
  search: string
}

export type OnFilterChange = (filter: Filter) => void

interface FilterBarProps {
  categories: Category[]
  filter: Filter
  onChange: OnFilterChange
}

export const FilterBar: FC<FilterBarProps> = ({
  categories,
  filter,
  onChange,
}) => {
  const handleSearchChange = (value: string) =>
    onChange({ ...filter, search: value })

  const handleCategoryChange = (value: string) =>
    onChange({ ...filter, category: value })

  const handleClear = () =>
    onChange({ category: '', search: '' })

  return (
    <div className={styles.filterBar} id="filter">
      {!!categories.length && (
        <>
          Category:
          <CategorySelect
            categories={categories}
            value={filter.category}
            onChange={handleCategoryChange}
          />
        </>
      )}

      Search:
      <DebouncedInput
        value={filter.search}
        onChange={handleSearchChange}
      />

      <button
        type="button"
        onClick={handleClear}
      >
        Clear
      </button>
    </div>
  )
}
