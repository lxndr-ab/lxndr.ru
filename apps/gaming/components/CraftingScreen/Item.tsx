import { FC } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { useRelativeLocation } from '../../utils/useRelativeLocation'
import styles from './Item.module.sass'
import { useGameData } from '../../providers/GameDataProvider'

interface ItemProps {
  itemId: string
  quantity: number | null
}

export const Item: FC<ItemProps> = ({ itemId, quantity }) => {
  const { calcRelRoute } = useRelativeLocation()
  const { findItemById } = useGameData()

  const it = findItemById(itemId)
  if (!it)
    throw new Error(`!!! ${itemId}`)

  const item = findItemById(itemId)
  const { title, iconFileHash } = item!

  return (
    <div className={styles.item}>
      <Link shallow className={styles.link} href={calcRelRoute({ search: title })}>
        <span className={styles.iconWrapper}>
          {iconFileHash &&
            <Image
              width={24}
              height={24}
              objectFit="contain"
              src={`${process.env.NEXT_PUBLIC_HOST}/api/files/${iconFileHash}`}
              alt={title}
            />
          }
        </span>

        <span className={styles.name}>
          {title}
        </span>
      </Link>

      {quantity && quantity > 1 && (
        <span className={styles.quantity}>&nbsp;(x{quantity})</span>
      )}
    </div>
  )
}
