import { FC } from 'react'
import Link from 'next/link'
import { useRelativeLocation } from '../../utils/useRelativeLocation'
import { formatSeconds } from '../../utils/formatSeconds'
import styles from './Effect.module.sass'
import { useGameData } from '../../providers/GameDataProvider'
import { ItemEffect } from '@prisma/client'

interface EffectProps {
  effect: ItemEffect
}

export const Effect: FC<EffectProps> = ({
  effect: { effectId, magnitude, duration },
}) => {
  const { calcRelRoute } = useRelativeLocation()
  const { findEffectById } = useGameData()
  const baseEffect = findEffectById(effectId)

  return (
    <div className={styles.effect}>
      &bull; &nbsp;

      <Link className={styles.name} shallow href={calcRelRoute({ search: baseEffect?.title })}>
        {baseEffect?.title || 'Unknown effect'}
      </Link>

      {magnitude && (
        <span className={styles.magnitude}> {magnitude} pts</span>
      )}

      {duration && (
        <span className={styles.duration}> for {formatSeconds(duration)}</span>
      )}
    </div>
  )
}
