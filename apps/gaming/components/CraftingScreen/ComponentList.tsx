import { FC } from 'react'
import { RecipeComponent } from '@prisma/client'
import { Item } from './Item'
import styles from './ComponentList.module.sass'

interface ComponentListProps {
  list: RecipeComponent[]
}

export const ComponentList: FC<ComponentListProps> = ({ list }) => (
  <div className={styles.componentList}>
    {list.map(item => <Item key={item.id} itemId={item.itemId} quantity={item.quantity} />)}
  </div>
)
