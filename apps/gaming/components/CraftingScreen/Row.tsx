import { FC } from 'react'
import Link from 'next/link'
import { Item } from './Item'
import { Effect } from './Effect'
import { ComponentList } from './ComponentList'
import { useRelativeLocation } from '../../utils/useRelativeLocation'
import styles from './Row.module.sass'
import { useGameData } from '../../providers/GameDataProvider'
import { Recipe } from '../../utils/game-data/game-data'

interface RowProps {
  recipe: Recipe
  columns: {
    effects?: boolean
    requirements?: boolean
    category?: boolean
  }
}

export const Row: FC<RowProps> = ({ recipe, columns }) => {
  const { calcRelRoute } = useRelativeLocation()
  const { findItemById, findRequirementById, findCategoryById } = useGameData()
  const category = findCategoryById(recipe.categoryId)
  const { effects } = findItemById(recipe.itemId)!

  return (
    <tr className={styles.row}>
      <td>
        <Item itemId={recipe.itemId} quantity={recipe.quantity} />
      </td>

      {columns.effects && (
        <td>
          {effects.map(effect => (
            <Effect key={effect.id} effect={effect} />
          ))}
        </td>
      )}

      <td>
        {recipe.components && (
          <ComponentList list={recipe.components} />
        )}
      </td>

      {columns.requirements && (
        <td>
          {recipe.requirements && recipe.requirements.map(recipeRequirement => {
            const req = findRequirementById(recipeRequirement.requirementId)!
            const value = [req.title, recipeRequirement.operator, recipeRequirement.value].join(' ')
            return <div key={value}>&bull; {value}</div>
          })}
        </td>
      )}

      {columns.category && (
        <td>
          {category && (
            <Link shallow className={styles.category} href={calcRelRoute({ category: recipe.categoryId })}>
              {category.title}
            </Link>
          )}
        </td>
      )}
    </tr>
  )
}
