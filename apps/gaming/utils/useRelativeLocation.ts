import omitBy from 'lodash/omitBy'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'

export const useRelativeLocation = <Q extends object>() => {
  const router = useRouter()

  const calcRelRoute = (nextQuery: Q) => {
    const query = { ...router.query, ...nextQuery }

    return {
      pathname: router.pathname,
      query: omitBy(query, isEmpty),
    }
  }

  const pushRelRoute = (nextQuery: Q) =>
    router.push(calcRelRoute(nextQuery), undefined, { shallow: true })

  return { calcRelRoute, pushRelRoute }
}
