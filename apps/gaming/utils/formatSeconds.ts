export const formatSeconds = (seconds: number): string => {
  const minutes = Math.floor(seconds / 60)
  seconds %= 60

  if (minutes > 0) {
    if (seconds === 0) {
      return `${minutes} mins`
    }

    return `${minutes} mins ${seconds} secs`
  }

  return `${seconds} secs`
}
