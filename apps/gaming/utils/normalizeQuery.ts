import type { ParsedUrlQuery } from 'querystring'

export const normalizeQuery = (query: ParsedUrlQuery) => {
  const ret: Record<string, string> = {}

  for (const [key, val] of Object.entries(query)) {
    if (val) {
      ret[key] = Array.isArray(val) ? val[0] : val
    }
  }

  return ret
}
