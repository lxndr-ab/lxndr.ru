import {
  PrismaClient,
  Game as PrismaGame,
  Mod,
  Effect,
  Item as PrismaItem,
  ItemEffect,
  Recipe as PrismaRecipe,
  Category,
  RecipeRequirement,
  RecipeComponent,
  Requirement,
} from '@prisma/client'

export type Item = PrismaItem & {
  effects: ItemEffect[]
}

export type Recipe = PrismaRecipe & {
  requirements: RecipeRequirement[]
  components: RecipeComponent[]
  searchIndex: string[]
}

export interface Game extends PrismaGame {
  mods: Mod[]
  requirements: Requirement[]
  categories: Category[]
  effects: Effect[]
  items: Item[]
  recipes: Recipe[]
}

const prisma = new PrismaClient()

export const loadGame = async (gameId: string): Promise<Game> => {
  const game = await prisma.game.findUniqueOrThrow({
    where: { id: gameId },
    include: {
      mods: true,
      categories: true,
      requirements: true,
      effects: true,
      items: {
        include: {
          effects: {
            orderBy: {
              order: 'asc',
            },
          },
        },
        orderBy: {
          title: 'asc',
        },
      },
      recipes: {
        include: {
          requirements: true,
          components: {
            orderBy: {
              order: 'asc',
            },
          },
        },
        orderBy: {
          item: {
            title: 'asc',
          },
        },
      },
    },
  })

  const gameData = game as Game

  return gameData
}
