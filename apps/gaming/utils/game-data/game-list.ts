import { PrismaClient, Game } from '@prisma/client'

const prisma = new PrismaClient()

export const loadGameList = async (): Promise<Game[]> => {
  const games = await prisma.game.findMany({
    orderBy: { title: 'asc' },
  })

  return games
}
