datasource db {
  url      = env("DB_URL")
  provider = "sqlite"
}

generator client {
  provider = "prisma-client-js"
}

model Game {
  id                 String              @id
  title              String              @unique
  type               String
  mods               Mod[]
  categories         Category[]
  requirements       Requirement[]
  effects            Effect[]
  items              Item[]
  itemEffects        ItemEffect[]
  recipes            Recipe[]
  recipeRequirements RecipeRequirement[]
}

model Mod {
  id          String
  gameId      String
  game        Game         @relation(fields: [gameId], references: [id])
  title       String
  abbr        String
  items       Item[]
  itemEffects ItemEffect[]
  recipes     Recipe[]

  @@id([gameId, id])
  @@unique([gameId, title])
  @@unique([gameId, abbr])
}

model Category {
  id      String
  gameId  String
  game    Game     @relation(fields: [gameId], references: [id])
  title   String
  recipes Recipe[]

  @@id([gameId, id])
  @@unique([gameId, title])
}

model Requirement {
  id      String
  title   String
  gameId  String
  game    Game                @relation(fields: [gameId], references: [id])
  recipes RecipeRequirement[]

  @@id([gameId, id])
  @@unique([gameId, title])
}

model Effect {
  id           String
  gameId       String
  game         Game         @relation(fields: [gameId], references: [id])
  title        String
  iconFileHash String?
  icon         File?        @relation(fields: [iconFileHash], references: [hash])
  itemEffects  ItemEffect[]

  @@id([gameId, id])
  @@unique([gameId, title])
}

model Item {
  id           String
  gameId       String
  game         Game         @relation(fields: [gameId], references: [id])
  modId        String
  mod          Mod          @relation(fields: [gameId, modId], references: [gameId, id])
  title        String
  description  String?
  iconFileHash String?
  icon         File?        @relation(fields: [iconFileHash], references: [hash])
  tag          String?
  price        Decimal?
  weight       Float?
  effects      ItemEffect[]
  recipes      Recipe[]

  @@id([gameId, modId, id])
  @@unique([gameId, modId, title])
}

model ItemEffect {
  id        Int     @id @default(autoincrement())
  gameId    String
  game      Game    @relation(fields: [gameId], references: [id])
  modId     String
  mod       Mod     @relation(fields: [gameId, modId], references: [gameId, id])
  itemId    String
  item      Item    @relation(fields: [gameId, modId, itemId], references: [gameId, modId, id])
  effectId  String
  effect    Effect  @relation(fields: [gameId, effectId], references: [gameId, id])
  order     Int?
  magnitude String?
  duration  Float?

  @@unique([gameId, modId, itemId, effectId, magnitude, duration])
}

model Recipe {
  id           Int                 @id @default(autoincrement())
  gameId       String
  game         Game                @relation(fields: [gameId], references: [id])
  modId        String
  mod          Mod                 @relation(fields: [gameId, modId], references: [gameId, id])
  itemId       String
  item         Item                @relation(fields: [gameId, modId, itemId], references: [gameId, modId, id])
  categoryId   String?
  category     Category?           @relation(fields: [gameId, categoryId], references: [gameId, id])
  quantity     Float
  requirements RecipeRequirement[]
  components   RecipeComponent[]
}

model RecipeComponent {
  id       Int    @id @default(autoincrement())
  recipeId Int
  recipe   Recipe @relation(fields: [recipeId], references: [id])
  itemId   String
  order    Int
  quantity Float?

  @@unique([recipeId, itemId, quantity])
}

model RecipeRequirement {
  id            Int         @id @default(autoincrement())
  gameId        String
  game          Game        @relation(fields: [gameId], references: [id])
  recipeId      Int
  recipe        Recipe      @relation(fields: [recipeId], references: [id])
  requirementId String
  requirement   Requirement @relation(fields: [gameId, requirementId], references: [gameId, id])
  operator      String?
  value         String?

  @@unique([requirementId, operator, value])
}

model File {
  hash    String   @id
  type    String
  size    Int
  data    Bytes
  effects Effect[]
  items   Item[]
}
