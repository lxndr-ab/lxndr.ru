data "docker_registry_image" "gaming" {
  name = "registry.gitlab.com/lxndr-ab/lxndr.ru/gaming:latest"
}

locals {
  gaming_labels = {
    "com.docker.stack.namespace"                                  = "lxndrru"
    "traefik.enable"                                              = "true"
    "traefik.http.services.lxndr_gaming.loadBalancer.server.port" = "3000"
    "traefik.http.routers.lxndr_gaming.rule"                      = "Host(`gaming.lxndr.ru`)"
    "traefik.http.routers.lxndr_gaming.middlewares"               = "redirect-to-https@file"
    "traefik.http.routers.lxndr_gaming.tls"                       = "true"
  }
}

resource "docker_service" "gaming" {
  name = "lxndrru_gaming"

  dynamic "labels" {
    for_each = local.gaming_labels
    content {
      label = labels.key
      value = labels.value
    }
  }

  task_spec {
    networks = [var.network_id]

    container_spec {
      image = "${data.docker_registry_image.gaming.name}@${data.docker_registry_image.gaming.sha256_digest}"

      labels {
        label = "com.docker.stack.namespace"
        value = "lxndrru"
      }

      healthcheck {
        test = ["CMD", "wget", "--quiet", "--spider", "http://localhost:3000/"]
      }
    }
  }
}
