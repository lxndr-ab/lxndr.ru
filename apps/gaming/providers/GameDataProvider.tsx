import { createContext, FC, ReactNode, useContext, useMemo } from 'react'
import { Category, Effect, Mod, Requirement } from '@prisma/client'
import uniq from 'lodash/uniq'
import sortBy from 'lodash/sortBy'
import { Game, Item, Recipe } from '../utils/game-data/game-data'
import recipes from '../pages/admin/[gameId]/[modId]/recipes'

interface ItemFilterOptions {
  effect?: string
}

interface RecipeFilterOptions extends ItemFilterOptions {
  category?: string
  search?: string
}

interface GameDataContextValue {
  game: Game
  availableMods: Mod[]
  selectedMods: Mod[]
  categories: Category[]
  effects: Effect[]
  items: Item[]
  findModById: (id: string) => Mod | null
  findRequirementById: (id: string) => Requirement | null
  findCategoryById: (id: string | null) => Category | null
  findEffectById: (id: string) => Effect | null
  findItemById: (id: string) => Item | null
  filterItems: (opts: ItemFilterOptions) => Item[]
  filterRecipes: (opts: RecipeFilterOptions) => Recipe[]
}

interface GameDataProviderProps {
  game: Game
  mods: string[] | null
  children: ReactNode
}

const BASE_MOD_ID = 'base'

export const isBaseMod = (mod: Mod | null): mod is null =>
  !mod || mod.id === BASE_MOD_ID

const normalizeModIds = (game: Game, modIDs: string[] | null) => {
  const ids = modIDs
    ? game.mods.filter(mod => modIDs.includes(mod.id)).map(mod => mod.id)
    : game.mods.map(mod => mod.id)
  return [BASE_MOD_ID, ...ids]
}

const filterItems = (items: Item[], opts: ItemFilterOptions = {}) => {
  let x = items

  if (opts.effect) {
    x = x.filter(item => {
      if (!item.effects) {
        return false
      }

      return item.effects.some(({ effectId }) => effectId === opts.effect)
    })
  }

  return x
}

const filterRecipes = (recipes: Recipe[], opts: RecipeFilterOptions = {}): Recipe[] => {
  let x = recipes

  if (opts.category) {
    x = x.filter(({ categoryId }) => categoryId === opts.category)
  }

  if (opts.search) {
    const search = opts.search.toLocaleLowerCase().trim()

    x = x.filter(
      ({ searchIndex }) => searchIndex.some(
        text => text.includes(search),
      ),
    )
  }

  return x
}

const GameDataContext = createContext<GameDataContextValue>({
  game: { id: '', title: '', type: '', mods: [], requirements: [], categories: [], effects: [], items: [], recipes: [] },
  availableMods: [],
  selectedMods: [],
  categories: [],
  effects: [],
  items: [],
  findModById: () => null,
  findRequirementById: () => null,
  findCategoryById: () => null,
  findEffectById: () => null,
  findItemById: () => null,
  filterItems: () => ([]),
  filterRecipes: () => ([]),
})

export const GameDataProvider: FC<GameDataProviderProps> = ({ game, mods, children }) => {
  const selectedMods = normalizeModIds(game, mods)

  const items = game.items.filter(item => selectedMods.includes(item.modId))
  const effectRefs = items.flatMap(item => item.effects).map(effectRef => effectRef.effectId)
  const effects = game.effects.filter(effect => effectRefs.includes(effect.id))

  const findModById = (id: string) =>
    game.mods.find(mod => mod.id === id) || null

  const findEffectById = (id: string) =>
    game.effects.find(effect => effect.id === id) || null

  const findItemById = (id: string) =>
    game.items.find(item => item.id === id) || null

  const findCategoryById = (id: string | null) =>
    game.categories.find(item => item.id === id) || null

  const findRequirementById = (id: string) =>
    game.requirements.find(req => req.id === id) || null

    const recipes: Recipe[] = game.recipes
    .filter(recipe => selectedMods.includes(recipe.modId))
    .map(recipe => {
      const item = findItemById(recipe.itemId)!

      const searchIndex = [
          item.title,
          ...item.effects.map(({ effectId }) => findEffectById(effectId)?.title),
          ...recipe.components.map(({ itemId }) => findItemById(itemId)?.title),
          ...recipe.components.map(({ itemId }) => findItemById(itemId)?.title),
          ...recipe.requirements.map(({ requirementId }) => findRequirementById(requirementId)?.title),
        ]
        .filter<string>((str): str is string => !!str)
        .map(str => str.toLocaleLowerCase())

      return {
        ...recipe,
        searchIndex: searchIndex,
      }
    })

  const categories = useMemo(() => {
    const ids = recipes.map(recipe => recipe.categoryId)
    const uniqIDs = uniq(ids)
    const categories = game.categories.filter(category => uniqIDs.includes(category.id))
    return sortBy(categories, 'title')
  }, [recipes])
  
  const ctx = useMemo(() => ({
    availableMods: game.mods.filter(mod => !isBaseMod(mod)),
    selectedMods: game.mods.filter(mod => !isBaseMod(mod)),
    game,
    categories,
    effects,
    items,
    findModById,
    findEffectById,
    findItemById,
    findRequirementById,
    findCategoryById,
    filterItems: (opts: ItemFilterOptions) => filterItems(items, opts),
    filterRecipes: (opts: RecipeFilterOptions) => filterRecipes(recipes, opts),
  }), [game, mods])

  return (
    <GameDataContext.Provider value={ctx}>
      {children}
    </GameDataContext.Provider>
  )
}

export const useGameData = () => useContext(GameDataContext)
