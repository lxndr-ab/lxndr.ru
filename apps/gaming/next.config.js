const withAntdLess = require('next-plugin-antd-less')

/** @type {import('next').NextConfig} */
module.exports = withAntdLess({
  reactStrictMode: true,
  output: 'standalone',
  images: {
    domains: ['localhost', 'gaming.lxndr.ru'],
  },
})
