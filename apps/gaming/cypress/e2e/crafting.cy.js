describe('Crafting', () => {
  it('should open', () => {
    cy.visit('/')
    cy
      .contains('The Elder Scrolls V (Crafting)')
      .click()
    cy
      .url()
      .should('include', '/tes5cr')
    cy
      .contains('Apple Pie')
      .should('exist')
  })

  it('should filter by category', () => {
    cy
      .get('#filter select')
      .select('Baking')
    cy
      .url({ timeout: 20000 })
      .should('include', 'category=baking')
  })

  it('should search', () => {
    cy
      .get('#filter input')
      .type('gar')
    cy
      .url()
      .should('include', 'search=gar')
    cy
      .get('#item-list tbody tr')
      .should('have.length', 8)
    cy
      .get('#item-list tbody tr:first-child td:first-child')
      .contains('Chicken Dumpling')
      .should('exist')
  })

  it('should clear search', () => {
    cy
      .get('#filter button')
      .click()
    cy
      .url()
      .should('include', '/tes5cr')
  })
})
