module.exports = {
  presets: [
    'next/babel',
  ],
  plugins: [
    'superjson-next',
  ],
  env: {
    test: {
      plugins: [
        'istanbul',
      ],
    },
  },
};
