import { GetStaticProps, NextPage } from 'next'
import Link from 'next/link'
import styles from './index.module.sass'
import { loadGameList } from '../utils/game-data/game-list'

interface HomePageProps {
  games: {
    id: string
    title: string
  }[]
}

export const getStaticProps: GetStaticProps<HomePageProps> = async () => {
  const games = await loadGameList()

  return {
    props: {
      games: games.map(({ id, title }) =>
        ({ id, title })),
    },
  }
}

const HomePage: NextPage<HomePageProps> = ({ games }) =>
  <main>
    <ul className={styles.gameList}>
      {games.map((game) => (
        <li key={game.id}>
          <Link href={`/${game.id}`}>
            {game.title}
          </Link>
        </li>
      ))}
    </ul>

    <footer className={styles.helpDesk}>
      If you have any questions or suggestions,
      <br />
      please use
      <a target="_blank" rel="noreferrer noopener" href="mailto:incoming+lxndr-ab-lxndr-ru-11946533-issue-@incoming.gitlab.com"> this email </a>
      or
      <a target="_blank" rel="noreferrer noopener" href="https://gitlab.com/lxndr-ab/lxndr.ru/-/issues"> issue tracker</a>
      .
    </footer>
  </main>

export default HomePage
