import { FC } from 'react'
import type { GetStaticProps, NextPage } from 'next'
import { useRouter } from 'next/router'
import Head from 'next/head'
import _ from 'lodash'
import { normalizeQuery } from '../utils/normalizeQuery'
import Header from '../components/Header'
import { AlchemyScreen } from '../components/AlchemyScreen/AlchemyScreen'
import { CraftingScreen } from '../components/CraftingScreen/CraftingScreen'
import { loadGameList } from '../utils/game-data/game-list'
import { Game, loadGame } from '../utils/game-data/game-data'
import { GameDataProvider } from '../providers/GameDataProvider'

type GamePageParams = {
  gameId: string
}

interface GamePageProps {
  game: Game
}

export const getStaticProps: GetStaticProps<GamePageProps, GamePageParams> = async ({ params }) => {
  const game = await loadGame(params!.gameId)

  return {
    props: { game },
  }
}

export const getStaticPaths = async () => {
  const games = await loadGameList()
  const paths = games.map(({ id }) => ({ params: { gameId: id } }))

  return {
    paths,
    fallback: false,
  }
}

const components: Record<string, FC> = {
  alchemy: AlchemyScreen,
  crafting: CraftingScreen,
}

const parseMods = (mods: string | undefined): string[] | null => {
  if (mods) {
    if (mods === 'none') {
      return []
    }

    return mods.split(',')
  }

  return null
}

const GamePage: NextPage<GamePageProps> = ({ game: gameData }) => {
  const router = useRouter()
  const query = normalizeQuery(router.query)
  const mods = parseMods(query.mods)

  const Component = components[gameData.type]

  if (!Component) {
    return null
  }

  return (
    <GameDataProvider game={gameData} mods={mods}>
      <Head>
        <title>{`${gameData.title} recipies`}</title>
      </Head>

      <Header />
      <Component />
    </GameDataProvider>
  )
}

export default GamePage
