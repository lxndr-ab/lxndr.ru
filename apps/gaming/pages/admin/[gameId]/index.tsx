import { AdminPage } from '../../../admin/AdminPage'
import { ModPage } from '../../../admin/ModPage'

const AdminModPage = () =>
  <AdminPage>
    <ModPage />
  </AdminPage>

export default AdminModPage
