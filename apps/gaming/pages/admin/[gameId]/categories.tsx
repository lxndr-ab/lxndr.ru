import { AdminPage } from '../../../admin/AdminPage'
import { CategoryPage } from '../../../admin/CategoryPage'

const AdminCategoryPage = () =>
  <AdminPage>
    <CategoryPage />
  </AdminPage>

export default AdminCategoryPage
