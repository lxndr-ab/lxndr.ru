import { AdminPage } from '../../../../admin/AdminPage'
import { RecipePage } from '../../../../admin/RecipePage'

const AdminRecipePage = () =>
  <AdminPage>
    <RecipePage />
  </AdminPage>

export default AdminRecipePage
