import { AdminPage } from '../../../../admin/AdminPage'
import { ItemPage } from '../../../../admin/ItemPage'

const AdminItemPage = () =>
  <AdminPage>
    <ItemPage />
  </AdminPage>

export default AdminItemPage
