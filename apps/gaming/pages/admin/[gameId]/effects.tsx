import { AdminPage } from '../../../admin/AdminPage'
import { EffectPage } from '../../../admin/EffectPage'

const AdminEffectPage = () =>
  <AdminPage>
    <EffectPage />
  </AdminPage>

export default AdminEffectPage
