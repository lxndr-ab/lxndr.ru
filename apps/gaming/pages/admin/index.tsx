import { AdminPage } from '../../admin/AdminPage'
import { GamePage } from '../../admin/GamePage'

const AdminGamePage = () =>
  <AdminPage>
    <GamePage />
  </AdminPage>

export default AdminGamePage
