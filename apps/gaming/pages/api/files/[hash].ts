import { PrismaClient } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import nc from 'next-connect'
import { HttpError,  NotFound } from 'http-errors'

const prisma = new PrismaClient()

const handleError = (err: HttpError, req: NextApiRequest, res: NextApiResponse) =>
  res.status(err.statusCode).end()

export default nc<NextApiRequest, NextApiResponse>({
  onError: handleError,
})
  .get(async (req, res) => {
    const file = await prisma.file.findUnique({
      where: { hash: req.query.hash as string },
    })

    if (!file) {
      throw new NotFound()
    }

    res
      .setHeader('Content-Type', file.type)
      .setHeader('Content-Length', file.size)
      .send(file.data)
  })
