import fs from 'node:fs/promises'
import { PrismaClient } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import nc from 'next-connect'
import { HttpError,  BadRequest, InternalServerError } from 'http-errors'
import  formidable  from  'formidable'

const prisma = new PrismaClient()

const handleError = (err: HttpError, req: NextApiRequest, res: NextApiResponse) =>
  res.status(err.statusCode).end()

export const config = {
  api: {
    bodyParser: false,
  },
}

export default nc<NextApiRequest, NextApiResponse>({
  onError: handleError,
})
  .post(async (req, res) => {
    const uploadedFile = await new Promise<formidable.File>((resolve, reject) => {
      const form = formidable({ hashAlgorithm: 'sha256' })

      form.parse(req, (err, fields, { file }) => {
        if (err) {
          reject(err)
          return
        }

        if (Array.isArray(file)) {
          reject(new TypeError('file mustn\'t be an array'))
          return
        }

        resolve(file)
      })
    })

    if (!uploadedFile.mimetype) {
      throw new BadRequest('file type could not be detected')
    }

    if (!uploadedFile.hash) {
      throw new InternalServerError('file has was not calculated')
    }

    const file = await prisma.file.upsert({
      where: {
        hash: uploadedFile.hash,
      },
      create: {
        hash: uploadedFile.hash,
        type: uploadedFile.mimetype,
        size: uploadedFile.size,
        data: await fs.readFile(uploadedFile.filepath),
      },
      update: {},
      select: {
        hash: true,
        type: true,
        size: true,
      },
    })

    res.json(file)
  })
