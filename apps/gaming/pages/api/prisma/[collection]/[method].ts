import { PrismaClient } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import nc from 'next-connect'
import { HttpError,  NotFound, MethodNotAllowed } from 'http-errors'

const allowedMethods = [
  'findMany',
  'upsert',
  'delete',
]

const prisma = new PrismaClient()

const handleError = (err: HttpError, req: NextApiRequest, res: NextApiResponse) => {
  console.log(err)

  if ('statusCode' in err) {
    res.status(err.statusCode).end()
  } else {
    res.status(500).json({
      message: err.message,
    })
  }
}

export default nc<NextApiRequest, NextApiResponse>({
  onError: handleError,
})
  .post(async (req, res) => {
    const collection = req.query.collection as string
    const method = req.query.method as string
    const query = req.body

    if (!(collection in prisma)) {
      throw new NotFound(`Collection ${collection} does not exist`)
    }

    if (collection.startsWith('$')) {
      throw new NotFound(`Collection ${collection} is invalid`)
    }

    const model = (prisma as any)[collection]

    if (!allowedMethods.includes(method)) {
      throw new MethodNotAllowed(`Method ${method} is not allowed`)
    }

    const entities = await model[method](query)

    res.json(entities)
  })
