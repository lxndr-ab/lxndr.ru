import { FC } from 'react'
import Script from 'next/script'
import type { AppProps } from 'next/app'
import SuperJSON from 'superjson'
import { Prisma } from '@prisma/client'
import './_app.sass'

SuperJSON.registerCustom<Prisma.Decimal, string>(
  {
    isApplicable: (v): v is Prisma.Decimal => Prisma.Decimal.isDecimal(v),
    serialize: v => v.toJSON(),
    deserialize: v => new Prisma.Decimal(v),
  },
  'decimal.js'
)

const GoogleAnalytics: FC = () => {
  const analyticsId = process.env.NEXT_PUBLIC_ANALYTICS_ID

  if (!analyticsId) {
    return null
  }

  return (
    <>
      <Script
        src={`https://www.googletagmanager.com/gtag/js?id=${analyticsId}`}
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '${analyticsId}');
        `}
      </Script>
    </>
  )
}

const App: FC<AppProps> = ({ Component, pageProps }) => (
  <>
    <GoogleAnalytics />
    <Component {...pageProps} />
  </>
)

export default App
