import { FC, useEffect, useState } from 'react'
import { Effect } from '@prisma/client'
import { Modal, Form, Input, InputNumber, Select, Typography, Button, Space } from 'antd'
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'

import { Item } from '../utils/game-data/game-data'
import { EntityModalProps } from './EntityPage'
import { GenericModal } from './ModalManager'
import { getEntities } from './api'
import { IconInput } from './IconInput'
import { IDInput } from './IDInput'

export interface ItemModelProps extends GenericModal, EntityModalProps<Item> {
  gameId: string
}

const tagOptions = [
  {
    value: 'quest',
    label: 'Quest item',
  },
  {
    value: 'uniqe',
    label: 'Unique item',
  }
]

export const ItemModal: FC<ItemModelProps> = ({
  gameId,
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Item>()
  const title = Form.useWatch('title', form)
  const [effects, setEffects] = useState<Effect[]>([])

  useEffect(() => {
    getEntities<Effect>('effect', { where: { gameId } })
      .then(setEffects)
  }, [gameId])

  const effectOptions = effects.map(effect => ({
    value: effect.id,
    label: effect.title,
  }))

  const handleSubmit = async () => {
    const item = await form.validateFields()
    await onSubmit(item)
  }

  return (
    <Modal visible title="Item" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        size="small"
        labelCol={{ span: 6, }}
        wrapperCol={{ span: 18 }}
      >
        <Form.Item
          hidden
          name="gameId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          hidden
          name="modId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="description"
          label="Description"
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          name="iconFileHash"
          label="Icon"
        >
          <IconInput />
        </Form.Item>

        <Form.Item
          name="price"
          label="Price"
        >
          <InputNumber  />
        </Form.Item>

        <Form.Item
          name="weight"
          label="Weight"
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          name="tag"
          label="Tag"
        >
          <Select
            allowClear
            options={tagOptions}
          />
        </Form.Item>

        <Typography.Title level={5}>Effects</Typography.Title>

        <Form.List name="effects">
          {(fields, { add, remove }) => (
            <>
              {fields.map(field => (
                <Space key={field.key} align="baseline">
                  <Form.Item hidden name={[field.name, 'id']}>
                    <Input />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    name={[field.name, 'effectId']}
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="Effect"
                      style={{ width: 200 }}
                      options={effectOptions}
                    />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    label="Magnitude"
                    name={[field.name, 'magnitude']}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    label="Duration"
                    name={[field.name, 'duration']}
                  >
                    <InputNumber />
                  </Form.Item>

                  <MinusCircleOutlined onClick={() => remove(field.name)} />
                </Space>
              ))}

              <Form.Item>
                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                  Add effect
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
      </Form>
    </Modal>
  )
}
