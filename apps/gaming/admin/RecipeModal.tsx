import { FC } from 'react'
import { Category, Requirement } from '@prisma/client'
import { Modal, Form, Input, InputNumber, Select, Typography, Button, Space } from 'antd'
import { CaretDownOutlined, CaretUpOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'

import { Item, Recipe } from '../utils/game-data/game-data'
import { EntityModalProps } from './EntityPage'
import { GenericModal } from './ModalManager'
import { useEntities } from './use-entities'

export interface RecipeModelProps extends GenericModal, EntityModalProps<Recipe> {
  gameId: string
}

export const RecipeModal: FC<RecipeModelProps> = ({
  gameId,
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Recipe>()

  const { data: items } = useEntities<Item>('item', { where: { gameId } })
  const { data: categories } = useEntities<Category>('category', { where: { gameId } })
  const { data: requirements } = useEntities<Requirement>('requirement', { where: { gameId } })

  const itemOptions = items.map(item => ({
    value: item.id,
    label: item.title,
  }))

  const requirementOptions = requirements.map(requirement => ({
    value: requirement.id,
    label: requirement.title,
  }))

  const categoryOptions = categories.map(category => ({
    value: category.id,
    label: category.title,
  }))

  const handleSubmit = async () => {
    const recipe = await form.validateFields()
    await onSubmit(recipe)
  }

  return (
    <Modal visible title="Recipe" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        size="small"
        labelCol={{ span: 6, }}
        wrapperCol={{ span: 18 }}
      >
        <Form.Item hidden name="id" />
        <Form.Item hidden name="gameId" />
        <Form.Item hidden name="modId" />

        <Form.Item
          name="itemId"
          label="Item"
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            placeholder="Item"
            options={itemOptions}
          />
        </Form.Item>

        <Form.Item
          name="quantity"
          label="Quantity"
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          name="categoryId"
          label="Category"
        >
          <Select allowClear options={categoryOptions} />
        </Form.Item>

        <Typography.Title level={5}>Components</Typography.Title>

        <Form.List name="components">
          {(fields, { add, remove, move }) =>
            <>
              {fields.map((field, index) =>
                <Space key={field.key} align="baseline">
                  <Form.Item hidden name={[field.name, 'id']} />
                  <Form.Item
                    {...field}
                    name={[field.name, 'itemId']}
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="Item"
                      style={{ width: 200 }}
                      options={itemOptions}
                    />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    label="Quantity"
                    name={[field.name, 'quantity']}
                  >
                    <InputNumber />
                  </Form.Item>

                  <Button disabled={index === 0} onClick={() => move(index, index - 1)} icon={<CaretUpOutlined />} />
                  <Button disabled={index >= fields.length - 1} onClick={() => move(index, index + 1)} icon={<CaretDownOutlined />} />
                  <Button danger type="primary" onClick={() => remove(field.name)} icon={<MinusCircleOutlined />} />
                </Space>
              )}

              <Form.Item>
                <Button type="dashed" onClick={() => add({ quantity: 1 })} icon={<PlusOutlined />}>
                  Add component
                </Button>
              </Form.Item>
            </>
          }
        </Form.List>

        <Typography.Title level={5}>Requirements</Typography.Title>

        <Form.List name="requirements">
          {(fields, { add, remove, move }) =>
            <>
              {fields.map((field, index) => (
                <Space key={field.key} align="baseline">
                  <Form.Item hidden name={[field.name, 'id']} />
                  <Form.Item
                    {...field}
                    name={[field.name, 'requirementId']}
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="Requirement"
                      style={{ width: 200 }}
                      options={requirementOptions}
                    />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    label="Operator"
                    name={[field.name, 'operator']}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    label="Value"
                    name={[field.name, 'value']}
                  >
                    <Input />
                  </Form.Item>

                  <Button disabled={index === 0} onClick={() => move(index, index - 1)} icon={<CaretUpOutlined />} />
                  <Button disabled={index >= fields.length - 1} onClick={() => move(index, index + 1)} icon={<CaretDownOutlined />} />
                  <Button danger type="primary" onClick={() => remove(field.name)} icon={<MinusCircleOutlined />} />
                </Space>
              ))}

              <Form.Item>
                <Button type="dashed" onClick={() => add()} icon={<PlusOutlined />}>
                  Add requirement
                </Button>
              </Form.Item>
            </>
          }
        </Form.List>
      </Form>
    </Modal>
  )
}
