import axios, { AxiosError } from 'axios'

export const apiRequest = axios.create();

apiRequest.interceptors.response.use(
  res => res,
  (err: AxiosError<{ message: string }>) => {
    if (err.isAxiosError) {
      err.message = err.response?.data?.message || err.message
    }

    return Promise.reject(err)
  },
)

export const getEntities = async <T>(collection: string, query: object = {}) => {
  const res = await apiRequest.post<T[]>(`/api/prisma/${collection}/findMany`, query)
  return res.data
}

export const upsertEntity = async <T>(collection: string, query: object) => {
  const res = await apiRequest.post<T>(`/api/prisma/${collection}/upsert`, query)
  return res.data
}

export const deleteEntity = async (collection: string, where: object) =>
  await apiRequest.post(`/api/prisma/${collection}/delete`, { where })
