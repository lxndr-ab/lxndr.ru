import { Menu, MenuProps } from 'antd'
import Link from 'next/link'
import { useRouter } from 'next/router'

export const MainMenu = () => {
  const router = useRouter()
  const { gameId, modId } = router.query

  const menuItems: MenuProps['items'] = [
    {
      key: '/admin',
      label: (
        <Link href="/admin">
          Games
        </Link>
      ),
    },
  ]

  if (gameId) {
    const path = `/admin/${gameId}`

    menuItems.push({
      key: path,
      label: (
        <Link href={path}>
          Mods
        </Link>
      ),
    })
  }

  if (gameId) {
    const effectsPath = `/admin/${gameId}/effects`

    menuItems.push({
      key: effectsPath,
      label: (
        <Link href={effectsPath}>
          Effects
        </Link>
      ),
    })
  }

  if (gameId) {
    const categoriesPath = `/admin/${gameId}/categories`

    menuItems.push({
      key: categoriesPath,
      label: (
        <Link href={categoriesPath}>
          Categories
        </Link>
      ),
    })
  }

  if (gameId) {
    const requirementsPath = `/admin/${gameId}/requirements`

    menuItems.push({
      key: requirementsPath,
      label: (
        <Link href={requirementsPath}>
          Requirements
        </Link>
      ),
    })
  }

  if (modId) {
    const itemsPath = `/admin/${gameId}/${modId}/items`

    menuItems.push({
      key: itemsPath,
      label: (
        <Link href={itemsPath}>
          Items
        </Link>
      ),
    })
  }

  if (modId) {
    const recipesPath = `/admin/${gameId}/${modId}/recipes`

    menuItems.push({
      key: recipesPath,
      label: (
        <Link href={recipesPath}>
          Recepies
        </Link>
      ),
    })
  }

  return (
    <Menu
      theme="dark"
      mode="horizontal"
      items={menuItems}
      selectedKeys={[router.asPath]}
    />
  )
}
