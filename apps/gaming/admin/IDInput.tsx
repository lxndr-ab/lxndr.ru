import { FC } from 'react'
import { Input, Button, Space } from 'antd'
import { RetweetOutlined } from '@ant-design/icons'
import slugify from 'slugify'

interface IDFieldProps {
  id: string
  idSource: string
  value?: string
  onChange?: (value: string) => void
}

export const IDInput: FC<IDFieldProps> = ({ id, idSource, value, onChange }) => {
  const handleGenerateID = () => {
    const id = slugify(idSource, { lower: true, strict: true })
    onChange?.(id)
  }

  return (
    <Space>
      <Input id={id} value={value} onChange={({ target }) => onChange?.(target.value)} />
      <Button
        icon={<RetweetOutlined />}
        onClick={handleGenerateID}
      />
    </Space>
  )
}
