import { Mod } from '@prisma/client'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { getEntities, upsertEntity } from './api'
import { EntityPage } from './EntityPage'
import { createActionColumn, createStringColumn } from './EntityPage/columns'
import { ModModal } from './ModModal'
import { gameRouteSchema } from './utils'

const COLLECTION_NAME = 'mod'

export const ModPage = () => {
  const { query, isReady } = useRouter()

  if (!isReady) {
    return null
  }

  const { gameId } = gameRouteSchema.validateSync(query)

  const newMod: Mod = {
    id: '',
    gameId,
    title: '',
    abbr: '',
  }

  const getPrimaryKey = ({ id, gameId }: Mod) => ({ gameId_id: { id, gameId } })

  const Mod = (mod: Mod) => ({
    ...mod,
    id: '',
    title: '',
    abbr: '',
  })

  const fetchFn = () =>
    getEntities<Mod>(COLLECTION_NAME, { where: { gameId } })

  const upsertFn = async (updatedMod: Mod) => {
    await upsertEntity<Mod>(COLLECTION_NAME, {
      where: getPrimaryKey(updatedMod),
      create: updatedMod,
      update: updatedMod,
    })
  }

  return (
    <EntityPage<Mod>
      collectionName={COLLECTION_NAME}
      EntityModal={ModModal}
      getPrimaryKey={getPrimaryKey}
      newEntity={newMod}
      duplicateEntity={Mod}
      fetchKey={{ gameId }}
      fetchFn={fetchFn}
      upsertFn={upsertFn}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        {
          title: 'ID',
          dataIndex: 'id',
          sorter: (a, b) => a.id.localeCompare(b.id),
          render: (id: string, mod: Mod) =>
            <Link href={`/admin/${mod.gameId}/${id}/items`}>
              {id}
            </Link>
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createStringColumn({
          title: 'Abbreviation',
          fieldName: 'abbr',
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
