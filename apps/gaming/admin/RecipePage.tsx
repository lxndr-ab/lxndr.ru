import { Category, Item, Requirement } from '@prisma/client'
import { useRouter } from 'next/router'
import { deleteEntity, getEntities, upsertEntity } from './api'
import { EntityPage } from './EntityPage'
import { createActionColumn, createNumberColumn } from './EntityPage/columns'
import { RecipeModal, RecipeModelProps } from './RecipeModal'
import { useEntities } from './use-entities'
import { modRouteSchema } from './utils'
import { Recipe } from '../utils/game-data/game-data'

const COLLECTION_NAME = 'recipe'

export const RecipePage = () => {
  const { query, isReady } = useRouter()

  if (!isReady) {
    return null
  }

  const { gameId, modId } = modRouteSchema.validateSync(query)

  const { data: items } = useEntities<Item>('item', { where: { gameId } })
  const { data: categories } = useEntities<Category>('category', { where: { gameId } })
  const { data: requirements } = useEntities<Requirement>('requirement', { where: { gameId } })

  const newRecipe: Recipe = {
    id: 0,
    gameId,
    modId,
    itemId: '',
    categoryId: null,
    quantity: 1,
    requirements: [],
    components: [],
    searchIndex: [],
  }

  const getItem = (itemId: string) => items.find(({ id }) => id === itemId)

  const getCategory = (categoryId?: string) => categories.find(({ id }) => id === categoryId)

  const getRequirement = (requirementId: string) => requirements.find(({ id }) => id === requirementId)!

  const getPrimaryKey = ({ id }: Recipe) => ({ id })

  const duplicateRecipe = (recipe: Recipe): Recipe => ({
    ...recipe,
    id: 0,
    requirements: recipe.requirements.map(requirement => ({
      ...requirement,
      id: 0,
      recipeId: 0,
    })),
    components: recipe.components.map(component => ({
      ...component,
      id: 0,
      recipeId: 0,
    })),
  })

  const fetchFn = () =>
    getEntities<Recipe>(COLLECTION_NAME, {
      where: { gameId, modId },
      include: {
        item: true,
        category: true,
        components: {
          orderBy: {
            order: 'asc',
          },
        },
        requirements: true,
      },
    })

  const upsertFn = async (
    { requirements, components, id, ...updatedRecipe }: Recipe,
    originalRecipe: Recipe,
  ) => {
    const { id: recipeId } = await upsertEntity<Recipe>('recipe', {
      where: { id: id || 0 },
      create: updatedRecipe,
      update: updatedRecipe,
    })

    // removed components
    for (const component of originalRecipe.components) {
      const updatedComponent = components.find(({ id }) => id === component.id)

      if (!updatedComponent) {
        await deleteEntity('recipeComponent', {
          id: component.id,
        })
      }
    }

    // create/update components
    for (const [index, { id, ...updatedComponent }] of Array.from(components.entries())) {
      const finalComponent = {
        ...updatedComponent,
        recipeId,
        itemId: updatedComponent.itemId,
        order: index + 1,
      }

      await upsertEntity('recipeComponent', {
        where: { id: id || 0 },
        create: finalComponent,
        update: finalComponent,
      })
    }
  }

  return (
    <EntityPage<Recipe, RecipeModelProps>
      collectionName={COLLECTION_NAME}
      EntityModal={RecipeModal}
      entityModalProps={{ gameId }}
      getPrimaryKey={getPrimaryKey}
      newEntity={newRecipe}
      duplicateEntity={duplicateRecipe}
      fetchKey={{ gameId, modId }}
      fetchFn={fetchFn}
      upsertFn={upsertFn}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createNumberColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        {
          title: 'Item',
          dataIndex: 'itemId',
          render: (itemId: string, recipe: Recipe) => {
            const item = getItem(itemId)
            return <>{item?.title} (x{recipe.quantity})</>
          },
          defaultSortOrder: 'ascend',
          sorter: (a, b) => {
            const titleA = getItem(a.itemId)?.title || ''
            const titleB = getItem(b.itemId)?.title || ''
            return titleA.localeCompare(titleB)
          },
        },
        {
          title: 'Category',
          dataIndex: 'categoryId',
          render: (categoryId?: string) => {
            const category = getCategory(categoryId)
            return category?.title
          },
        },
        {
          title: 'Components',
          dataIndex: 'components',
          render: (components: Recipe["components"]) =>
            <ul>
              {components.map(({ id, itemId, quantity }) => {
                const item = getItem(itemId)
                return <li key={id}>{item?.title} (x{quantity})</li>
              })}
            </ul>
        },
        {
          title: 'Requirements',
          dataIndex: 'requirements',
          render: (requirements: Recipe["requirements"]) =>
            <ul>
              {requirements.map(({ id, requirementId, operator, value }) => {
                const requirement = getRequirement(requirementId)
                return <li key={id}>{requirement.title} {operator} {value}</li>
              })}
            </ul>
        },
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
