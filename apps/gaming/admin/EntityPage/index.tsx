import { FC } from 'react'
import { useQuery, useMutation } from 'react-query'
import { AxiosError } from 'axios'
import { notification, Button, Table, Modal } from 'antd'
import { ColumnsType } from 'antd/lib/table'
import { ExclamationCircleOutlined } from '@ant-design/icons'

import { useModal } from '../ModalManager'
import { deleteEntity } from '../api'
import { GenericEntity } from './type'
import { CreateActionColumn } from './columns'

export interface EntityModalProps<T extends GenericEntity> {
  initialValues?: T
  onSubmit: (updatedEntity: T) => Promise<void>
}

interface EntityPageProps<
  T extends GenericEntity,
  MODAL_PROPS extends EntityModalProps<T> = EntityModalProps<T>,
> {
  collectionName: string
  EntityModal: FC<MODAL_PROPS>
  entityModalProps?: Partial<MODAL_PROPS>
  getPrimaryKey: (entity: T) => object
  createColumns: (options: CreateActionColumn<T>) => ColumnsType<T>
  newEntity: T
  duplicateEntity: (entity: T) => T
  fetchKey?: unknown
  fetchFn: () => Promise<T[]>
  upsertFn: (updatedEntity: T, originalEntity: T) => Promise<void>
}

const handleApiRequestError = (err: AxiosError<{ message: string}>) =>
  notification.error({
    message: 'Error',
    description: (
      <pre>
        {err.response?.data.message}
      </pre>
    ),
  })

export function EntityPage<
  T extends GenericEntity,
  MODAL_PROPS extends EntityModalProps<T> = EntityModalProps<T>,
>({
  collectionName,
  EntityModal,
  entityModalProps = {},
  getPrimaryKey,
  createColumns,
  newEntity,
  duplicateEntity,
  fetchKey,
  fetchFn,
  upsertFn,
}: EntityPageProps<T, MODAL_PROPS>) {
  const { openModal } = useModal()

  const entityQuery = useQuery({
    queryKey: [collectionName, fetchKey],
    queryFn: fetchFn,
    initialData: [],
    onError: handleApiRequestError,
  })

  const upsertMutation = useMutation({
    mutationKey: [collectionName, 'upsert'],
    mutationFn: (variables: [T, T]) => upsertFn(...variables),
    onError: handleApiRequestError,
  })

  const deleteMutation = useMutation({
    mutationKey: [collectionName, 'delete'],
    mutationFn: (entity: T) => deleteEntity(collectionName, getPrimaryKey(entity)),
    onError: handleApiRequestError,
  })

  const openEntityModal = (entity?: T) => {
    // @ts-ignore
    const closeModal = openModal(EntityModal, {
      ...entityModalProps,
      initialValues: entity,
      onSubmit: async (updatedEntity) => {
        await upsertMutation.mutateAsync([updatedEntity, entity || newEntity])
        await entityQuery.refetch()
        closeModal()
      },
    })
  }

  const handleAddEntity = () =>
    openEntityModal(newEntity)

  const handleEditEntity = (entity: T) =>
    openEntityModal(entity)

  const handleDuplicateEntity = (entity: T) =>
    openEntityModal(duplicateEntity(entity))

  const handleDeleteEntity = (entity: T) => {
    Modal.confirm({
      title: 'Confirmation',
      icon: <ExclamationCircleOutlined />,
      content: 'Are you sure you want to delete this item?',
      onOk: async () => {
        await deleteMutation.mutateAsync(entity)
        await entityQuery.refetch()
      },
    })
  }

  return (
    <>
      <Button
        type="primary"
        style={{ margin: '16px'}}
        onClick={handleAddEntity}
      >
        Add new {collectionName}
      </Button>

      <Table<T>
        dataSource={entityQuery.data}
        rowKey={entity => JSON.stringify(getPrimaryKey(entity))}
        pagination={false}
        size="small"
        columns={createColumns({
          onEditClick: handleEditEntity,
          onDuplicateClick: handleDuplicateEntity,
          onDeleteClick: handleDeleteEntity,
        })}
      />
    </>
  )
}
