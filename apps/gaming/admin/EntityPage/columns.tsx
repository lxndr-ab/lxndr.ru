import { CopyOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Space } from 'antd'
import { ColumnType } from 'antd/lib/table'

import { GenericEntity } from './type'

interface CreateSortingColumnOptions {
  title: string
  fieldName: string
  defaultSort?: boolean
}

export const createStringColumn = <T extends GenericEntity>({
  title,
  fieldName,
  defaultSort,
}: CreateSortingColumnOptions): ColumnType<T> => ({
  title,
  dataIndex: fieldName,
  defaultSortOrder: defaultSort ? 'ascend' : undefined,
  sorter: (a: any, b: any) => a[fieldName].localeCompare(b[fieldName]),
})

export const createNumberColumn = <T extends GenericEntity>({
  title,
  fieldName,
  defaultSort,
}: CreateSortingColumnOptions): ColumnType<T> => ({
  title,
  dataIndex: fieldName,
  defaultSortOrder: defaultSort ? 'ascend' : undefined,
  sorter: (a: any, b: any) => a[fieldName] - b[fieldName],
})

export interface CreateActionColumn<T extends GenericEntity> {
  onEditClick: (entity: T) => void
  onDuplicateClick: (entity: T) => void
  onDeleteClick: (entity: T) => void
}

export const createActionColumn = <T extends GenericEntity>({
  onEditClick,
  onDuplicateClick,
  onDeleteClick,
}: CreateActionColumn<T>) => ({
  key: 'actions',
  render: (value: unknown, entity: T) =>
    <Space>
      <Button icon={<EditOutlined />} title="Edit" onClick={() => onEditClick(entity)} />
      <Button icon={<CopyOutlined />} title="Duplicate" onClick={() => onDuplicateClick(entity)} />
      <Button icon={<DeleteOutlined />} title="Delete" onClick={() => onDeleteClick(entity)} />
    </Space>
})
