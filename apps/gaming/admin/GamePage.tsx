import { Game } from '@prisma/client'
import Link from 'next/link'
import { getEntities, upsertEntity } from './api'
import { EntityPage } from './EntityPage'
import { createActionColumn, createStringColumn } from './EntityPage/columns'
import { GameModal } from './GameModal'

const COLLECTION_NAME = 'game'

export const GamePage = () => {
  const getPrimaryKey = (game: Game) => ({ id: game.id })

  const newGame: Game = {
    id: '',
    title: '',
    type: 'crafting',
  }

  const duplicateItem = (game: Game) => ({
    ...game,
    id: '',
    title: '',
  })

  const fetchFn = () =>
    getEntities<Game>(COLLECTION_NAME)

  const upsertFn = async (updatedGame: Game) => {
    await upsertEntity<Game>(COLLECTION_NAME, {
      where: getPrimaryKey(updatedGame),
      create: updatedGame,
      update: updatedGame,
    })
  }

  return (
    <EntityPage<Game>
      collectionName={COLLECTION_NAME}
      EntityModal={GameModal}
      getPrimaryKey={getPrimaryKey}
      newEntity={newGame}
      duplicateEntity={duplicateItem}
      fetchFn={fetchFn}
      upsertFn={upsertFn}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        {
          title: 'ID',
          dataIndex: 'id',
          sorter: (a, b) => a.id.localeCompare(b.id),
          render: (id: string) =>
            <Link href={`/admin/${id}`}>
              {id}
            </Link>
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createStringColumn({
          title: 'Type',
          fieldName: 'type',
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
