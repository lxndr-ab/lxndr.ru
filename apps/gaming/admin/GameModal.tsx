import { FC } from 'react'
import { Game } from '@prisma/client'
import { Modal, Form, Input, Select } from 'antd'
import { GenericModal } from './ModalManager'
import { IDInput } from './IDInput'
import { EntityModalProps } from './EntityPage'

interface GameModelProps extends GenericModal, EntityModalProps<Game> {}

export const GameModal: FC<GameModelProps> = ({
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Game>()
  const title = Form.useWatch('title', form)

  const typeOptions = [
    {
      value: 'alchemy',
      label: 'Alchemy',
    },
    {
      value: 'crafting',
      label: 'Crafting',
    }
  ]

  const handleSubmit = async () => {
    await form.validateFields()
    await onSubmit(form.getFieldsValue())
  }

  return (
    <Modal visible title="Game" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="type"
          label="Type"
          rules={[
            { required: true },
          ]}
        >
          <Select options={typeOptions} />
        </Form.Item>
      </Form>
    </Modal>
  )
}
