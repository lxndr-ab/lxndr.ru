import * as yup from 'yup'

export const gameRouteSchema = yup.object({
  gameId: yup.string().required(),
})

export const modRouteSchema = yup.object({
  gameId: yup.string().required(),
  modId: yup.string().required(),
})
