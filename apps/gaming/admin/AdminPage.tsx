import { FC, ReactNode } from 'react'
import { Layout } from 'antd'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ModalProvider } from './ModalManager'
import { MainMenu } from './MainMenu'

import 'antd/lib/style'
import 'antd/lib/layout/style'
import 'antd/lib/notification/style'
import 'antd/lib/table/style'
import 'antd/lib/button/style'
import 'antd/lib/space/style'
import 'antd/lib/form/style'
import 'antd/lib/modal/style'
import 'antd/lib/input/style'
import 'antd/lib/input-number/style'
import 'antd/lib/upload/style'
import 'antd/lib/row/style'
import 'antd/lib/col/style'
import 'antd/lib/typography/style'
import 'antd/lib/menu/style'
import 'antd/lib/spin/style'

const queryClient = new QueryClient()
const { Header, Content } = Layout

interface AdminPageProps {
  children: ReactNode
}

export const AdminPage: FC<AdminPageProps> = ({ children }) => {
  return (
    <QueryClientProvider client={queryClient}>
      <ModalProvider>
        <Layout>
          <Header>
            <MainMenu />
          </Header>

          <Content>
            {children}
          </Content>
        </Layout>
      </ModalProvider>
    </QueryClientProvider>
  )
}
