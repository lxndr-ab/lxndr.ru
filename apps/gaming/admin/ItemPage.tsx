import { ItemEffect, Item as PrismaItem } from '@prisma/client'
import { useRouter } from 'next/router'
import Image from 'next/legacy/image'
import { deleteEntity, getEntities, upsertEntity } from './api'
import { EntityPage } from './EntityPage'
import { createActionColumn, createStringColumn, createNumberColumn } from './EntityPage/columns'
import { ItemModal, ItemModelProps } from './ItemModal'
import { Item } from '../utils/game-data/game-data'
import { modRouteSchema } from './utils'

const COLLECTION_NAME = 'item'

export const ItemPage = () => {
  const { query, isReady } = useRouter()

  if (!isReady) {
    return null
  }

  const { gameId, modId } = modRouteSchema.validateSync(query)
  const filter = { gameId, modId }

  const newItem: Item = {
    gameId,
    modId,
    id: '',
    title: '',
    description: null,
    iconFileHash: null,
    tag: null,
    price: null,
    weight: null,
    effects: [],
  }

  const getPrimaryKey = ({ id, gameId, modId }: PrismaItem) => ({ gameId_modId_id: { id, gameId, modId } })

  const duplicateItem = (item: Item) => ({
    ...item,
    id: '',
    effects: item.effects.map(effect => ({
      ...effect,
      id: 0,
    })),
  })

  const fetchFn = () =>
    getEntities<Item>(COLLECTION_NAME, {
      where: filter,
      include: {
        effects: true,
      },
    })

  const upsertFn = async ({ effects, ...updatedItem }: Item, originalItem: Item) => {
    await upsertEntity<Item>(COLLECTION_NAME, {
      where: getPrimaryKey(updatedItem),
      create: updatedItem,
      update: updatedItem,
    })

    // removed effects
    for (const effect of originalItem.effects) {
      const updatedEffect = effects.find(({ id }) => id === effect.id)

      if (!updatedEffect) {
        await deleteEntity('itemEffect', { id: effect.id })
      }
    }

    // create/update effects
    for (const [index, updatedEffect] of Array.from(effects.entries())) {
      const finalEffect = {
        ...updatedEffect,
        gameId,
        modId,
        itemId: updatedItem.id,
        order: index + 1,
      }

      await upsertEntity<ItemEffect>('itemEffect', {
        where: { id: updatedEffect.id || 0 },
        create: { ...finalEffect },
        update: { ...finalEffect },
      })
    }
  }

  return (
    <EntityPage<Item, ItemModelProps>
      collectionName={COLLECTION_NAME}
      EntityModal={ItemModal}
      getPrimaryKey={getPrimaryKey}
      newEntity={newItem}
      duplicateEntity={duplicateItem}
      fetchKey={filter}
      fetchFn={fetchFn}
      upsertFn={upsertFn}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createNumberColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        {
          title: 'Icon',
          dataIndex: 'iconFileHash',
          render: (iconFileHash) => (
            iconFileHash &&
              <Image
                width={24}
                height={24}
                objectFit="contain"
                src={`${process.env.NEXT_PUBLIC_HOST}/api/files/${iconFileHash}`}
              />
          ),
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createNumberColumn({
          title: 'Price',
          fieldName: 'price',
        }),
        createNumberColumn({
          title: 'Weight',
          fieldName: 'weight',
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
