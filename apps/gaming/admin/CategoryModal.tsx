import { FC } from 'react'
import { Category } from '@prisma/client'
import { Modal, Form, Input } from 'antd'
import { GenericModal } from './ModalManager'
import { IDInput } from './IDInput'
import { EntityModalProps } from './EntityPage'

interface CategoryModalProps extends GenericModal, EntityModalProps<Category> {}

export const CategoryModal: FC<CategoryModalProps> = ({
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Category>()
  const title = Form.useWatch('title', form)

  const handleSubmit = async () => {
    const category = await form.validateFields()
    await onSubmit(category)
  }

  return (
    <Modal visible title="Category" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <Form.Item
          hidden
          name="gameId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  )
}
