import { FC } from 'react'
import { Mod } from '@prisma/client'
import { Modal, Form, Input } from 'antd'
import { GenericModal } from './ModalManager'
import { IDInput } from './IDInput'
import { EntityModalProps } from './EntityPage'

interface ModModelProps extends GenericModal, EntityModalProps<Mod> {}

export const ModModal: FC<ModModelProps> = ({
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Mod>()
  const title = Form.useWatch('title', form)

  const handleSubmit = async () => {
    const mod = await form.validateFields()
    await onSubmit(mod)
  }

  return (
    <Modal visible title="Mod" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <Form.Item
          hidden
          name="gameId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="abbr"
          label="Abbreviation"
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  )
}
