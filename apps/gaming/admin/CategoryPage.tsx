import { Category } from '@prisma/client'
import { useRouter } from 'next/router'
import { getEntities, upsertEntity } from './api'
import { EntityPage } from './EntityPage'
import { createActionColumn, createStringColumn } from './EntityPage/columns'
import { CategoryModal } from './CategoryModal'
import { gameRouteSchema } from './utils'

const COLLECTION_NAME = 'category'

export const CategoryPage = () => {
  const { query, isReady } = useRouter()

  if (!isReady) {
    return null
  }

  const { gameId } = gameRouteSchema.validateSync(query)

  const newCategory: Category = {
    gameId,
    id: '',
    title: '',
  }

  const getPrimaryKey = ({ id, gameId }: Category) => ({ gameId_id: { id, gameId } })

  const duplicateEntity = (category: Category) => ({
    ...category,
    id: '',
    title: '',
  })

  const fetchFn = () =>
    getEntities<Category>(COLLECTION_NAME, { where: { gameId } })

  const upsertFn = async (updatedCategory: Category) => {
    await upsertEntity<Category>(COLLECTION_NAME, {
      where: getPrimaryKey(updatedCategory),
      create: updatedCategory,
      update: updatedCategory,
    })
  }

  return (
    <EntityPage<Category>
      collectionName={COLLECTION_NAME}
      EntityModal={CategoryModal}
      getPrimaryKey={getPrimaryKey}
      newEntity={newCategory}
      duplicateEntity={duplicateEntity}
      fetchKey={{ gameId }}
      fetchFn={fetchFn}
      upsertFn={upsertFn}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createStringColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
