import { Effect } from '@prisma/client'
import { useRouter } from 'next/router'
import Image from 'next/legacy/image'
import { getEntities, upsertEntity } from './api'
import { EntityPage } from './EntityPage'
import { createActionColumn, createStringColumn } from './EntityPage/columns'
import { EffectModal } from './EffectModal'
import { gameRouteSchema } from './utils'

const COLLECTION_NAME = 'effect'

export const EffectPage = () => {
  const { query, isReady } = useRouter()

  if (!isReady) {
    return null
  }

  const { gameId } = gameRouteSchema.validateSync(query)

  const newEffect: Effect = {
    gameId,
    id: '',
    title: '',
    iconFileHash: null,
  }

  const getPrimaryKey = ({ id, gameId }: Effect) => ({ gameId_id: { id, gameId } })

  const duplicateEntity = (effect: Effect) => ({
    ...effect,
    id: '',
    title: '',
    iconFileHash: null,
  })

  const fetchFn = () =>
    getEntities<Effect>(COLLECTION_NAME, { where: { gameId } })

  const upsertFn = async (updatedEffect: Effect) => {
    await upsertEntity<Effect>(COLLECTION_NAME, {
      where: getPrimaryKey(updatedEffect),
      create: updatedEffect,
      update: updatedEffect,
    })
  }

  return (
    <EntityPage<Effect>
      collectionName={COLLECTION_NAME}
      EntityModal={EffectModal}
      getPrimaryKey={getPrimaryKey}
      newEntity={newEffect}
      duplicateEntity={duplicateEntity}
      fetchKey={{ gameId }}
      fetchFn={fetchFn}
      upsertFn={upsertFn}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createStringColumn({
          title: 'ID',
          fieldName: 'id',
          defaultSort: true,
        }),
        {
          title: 'Icon',
          dataIndex: 'iconFileHash',
          render: (iconFileHash) => (
            iconFileHash &&
              <Image
                width={24}
                height={24}
                objectFit="contain"
                src={`${process.env.NEXT_PUBLIC_HOST}/api/files/${iconFileHash}`}
              />
          ),
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
