import { FC } from 'react'
import { Effect } from '@prisma/client'
import { Modal, Form, Input } from 'antd'
import { GenericModal } from './ModalManager'
import { IDInput } from './IDInput'
import { IconInput } from './IconInput'
import { EntityModalProps } from './EntityPage'

interface EffectModalProps extends GenericModal, EntityModalProps<Effect> {}

export const EffectModal: FC<EffectModalProps> = ({
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Effect>()
  const title = Form.useWatch('title', form)

  const handleSubmit = async () => {
    const effect = await form.validateFields()
    await onSubmit(effect)
  }

  return (
    <Modal visible title="Effect" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <Form.Item
          hidden
          name="gameId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="iconFileHash"
          label="Icon"
        >
          <IconInput />
        </Form.Item>
      </Form>
    </Modal>
  )
}
