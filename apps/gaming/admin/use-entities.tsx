import { notification } from 'antd';
import { useQuery } from 'react-query'
import { getEntities } from './api'

export const useEntities = <T extends object>(collectionName: string, query: object) => {
  const { data = [], refetch } = useQuery<T[]>({
    queryKey: [collectionName, query],
    queryFn: () => getEntities<T>(collectionName, query),
    initialData: [],
    onError: (err: any) => {
      notification.error({
        message: `Error fetching ${collectionName} list`,
        description: (
          <pre>
            {err.response?.data.message}
          </pre>
        ),
      })
    },
  })

  return { data, refetch }
}
